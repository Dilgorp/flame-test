import 'package:flame/game.dart';
import 'package:flame_test/controllers/construction_controller.dart';
import 'package:flame_test/controllers/zoom_controller.dart';
import 'package:flame_test/designer.dart';
import 'package:flame_test/widgets/element_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DesignerPage extends StatelessWidget {
  final RxBool _increasePressed = false.obs;
  final RxBool _decreasePressed = false.obs;

  final ConstructionController _constructionController = Get.find();

  DesignerPage({Key? key}) : super(key: key) {
    _constructionController.subscribeTappedElements((tappedElements) async {
      // designer.showOverlay('ElementChosenDialog');
    });
  }

  final ZoomController _zoomController = Get.find();
  final Designer designer = Designer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flame test"),
      ),
      body: Stack(children: [
        GameWidget(
          game: designer,
          overlayBuilderMap: {
            'ElementChosenDialog': _elementDialogBuilder,
          },
        ),
        Positioned(
          bottom: 60,
          right: 20,
          child: GestureDetector(
            onTapDown: (_) async {
              _increasePressed.value = true;
              while (_increasePressed.value) {
                await Future.delayed(const Duration(milliseconds: 16));
                _increaseZoom();
              }
            },
            onTapUp: (_) {
              _increasePressed.value = false;
            },
            onTapCancel: () {
              _increasePressed.value = false;
            },
            child: IconButton(
              icon: const Icon(Icons.add_circle_outline),
              onPressed: () {
                _increaseZoom();
              },
            ),
          ),
        ),
        Positioned(
          bottom: 20,
          right: 20,
          child: GestureDetector(
            onTapDown: (_) async {
              _decreasePressed.value = true;
              while (_decreasePressed.value) {
                await Future.delayed(const Duration(milliseconds: 16));
                _decreaseZoom();
              }
            },
            onTapUp: (_) {
              _decreasePressed.value = false;
            },
            onTapCancel: () {
              _decreasePressed.value = false;
            },
            child: IconButton(
              icon: const Icon(Icons.remove_circle_outline),
              onPressed: () {
                _decreaseZoom();
              },
            ),
          ),
        ),
      ]),
    );
  }

  void _increaseZoom() {
    _zoomController.increase();
  }

  void _decreaseZoom() {
    _zoomController.decrease();
  }

  Widget _elementDialogBuilder(BuildContext buildContext, FlameGame game) {
    return ElementDialog(
      parts: _constructionController.tappedElements(),
      designer: designer,
    );
  }
}
