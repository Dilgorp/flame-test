import 'package:flame_test/components/frame_beam.dart';
import 'package:flame_test/designer.dart';
import 'package:flutter/material.dart';

class ElementDialog extends StatelessWidget {
  final List<FrameBeam> parts;
  final Designer designer;

  const ElementDialog({
    Key? key,
    required this.parts,
    required this.designer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: parts.length + 1,
      itemBuilder: (context, index) {
        if (index == parts.length) {
          return GestureDetector(
            onTap: () {
              designer.hideOverlay();
            },
            child: const Text('Close'),
          );
        }

        return Text(parts[index].toString());
      },
    );
  }
}
