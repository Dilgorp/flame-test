
import 'package:flame_test/controllers/world_size_controller.dart';
import 'package:flame_test/controllers/zoom_controller.dart';
import 'package:flame_test/widgets/designer_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/camera_controller.dart';
import 'controllers/construction_controller.dart';

import 'package:get/route_manager.dart';
void main() {
  Get.put<CameraController>(
    CameraControllerImpl(),
    permanent: true,
  );
  Get.put<ConstructionController>(
    ConstructionControllerImpl(),
    permanent: true,
  );
  Get.put<WorldSizeController>(
    WorldSizeControllerImpl(),
    permanent: true,
  );

  Get.put<ZoomController>(
    ZoomControllerImpl(),
    permanent: true,
  );

  runApp(const GetMaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'flame test',
    home: FlameTest(),
  ));
}

class FlameTest extends StatelessWidget {
  const FlameTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DesignerPage(),
    );
  }
}
