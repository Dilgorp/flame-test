import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class SimpleConstruction extends PositionComponent {
  SimpleConstruction(Vector2 position)
      : super(
          position: position,
          size: Vector2(420, 220),
        );

  static final Paint fill = Paint()
    ..color = const Color(0xFFFFFFFF)
    ..style = PaintingStyle.fill
    ..strokeWidth = 1;

  static final Paint pressedFill = Paint()
    ..color = const Color(0xFF30C1FF)
    ..style = PaintingStyle.fill
    ..strokeWidth = 1;

  static final Paint border = Paint()
    ..color = const Color(0xFF000000)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1;

  @override
  Future<void>? onLoad() {
    addAll([
      LeftPart(Vector2(0, 0)),
      TopPart(Vector2(0, 0)),
      RightPart(Vector2(400, 0)),
      BottomPart(Vector2(0, 200)),
    ]);
    return super.onLoad();
  }
}

abstract class BasicPart extends PositionComponent with Tappable {
  bool _pressed = false;

  BasicPart(Vector2 position, Vector2 size)
      : super(
          position: position,
          size: size,
        );

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    final path = getPath();

    canvas.drawPath(
      path,
      _pressed ? SimpleConstruction.pressedFill : SimpleConstruction.fill,
    );

    canvas.drawPath(
      path,
      SimpleConstruction.border,
    );
  }

  Path getPath();

  @override
  bool onTapDown(TapDownInfo info) {
    _pressed = !_pressed;
    return true;
  }
}

class LeftPart extends BasicPart {
  LeftPart(Vector2 position) : super(position, Vector2(20, 220));

  @override
  Path getPath() => Path()
    ..moveTo(0, 0)
    ..lineTo(20, 20)
    ..lineTo(20, 200)
    ..lineTo(0, 220)
    ..lineTo(0, 0);
}

class RightPart extends BasicPart {
  RightPart(Vector2 position) : super(position, Vector2(20, 220));

  @override
  Path getPath() => Path()
    ..moveTo(20, 0)
    ..lineTo(0, 20)
    ..lineTo(0, 200)
    ..lineTo(20, 220)
    ..lineTo(20, 0);
}

class TopPart extends BasicPart {
  TopPart(Vector2 position) : super(position, Vector2(420, 20));

  @override
  Path getPath() => Path()
    ..moveTo(0, 0)
    ..lineTo(20, 20)
    ..lineTo(400, 20)
    ..lineTo(420, 0)
    ..lineTo(0, 0);
}

class BottomPart extends BasicPart {
  BottomPart(Vector2 position) : super(position, Vector2(420, 20));

  @override
  Path getPath() => Path()
    ..moveTo(0, 20)
    ..lineTo(20, 0)
    ..lineTo(400, 0)
    ..lineTo(420, 20)
    ..lineTo(0, 20);
}
