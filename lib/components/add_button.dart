import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class AddButton extends PositionComponent {
  bool _pressed = false;

  final bool drawBorder;
  final void Function()? onTap;

  static const double side = 50;

  final List<int> _tapPointers = [];

  AddButton(
    Vector2 position, {
    this.drawBorder = false,
    this.onTap,
  }) : super(
          position: position,
          size: Vector2.all(50),
        );

  static final Paint paint = Paint()
    ..color = const Color(0xFFADACAC)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 4;

  static final Paint border = Paint()
    ..color = const Color(0xFF000000)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1;

  static final Paint pressedPaint = Paint()
    ..color = const Color(0xFF30C1FF)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 4;

  @override
  void render(Canvas canvas) {
    super.render(canvas);

    final path = Path();
    path.addRRect(RRect.fromLTRBR(10, 10, 40, 40, const Radius.circular(5)));
    path.moveTo(25, 14);
    path.lineTo(25, 36);

    path.moveTo(14, 25);
    path.lineTo(36, 25);

    canvas.drawPath(
      path,
      _pressed ? pressedPaint : paint,
    );

    if (drawBorder) {
      final borderPath = Path();
      borderPath.addRect(const Rect.fromLTRB(0, 0, 50, 50));
      canvas.drawPath(
        borderPath,
        border,
      );
    }
  }

  void onTapDown(int pointerId, TapDownInfo info) {
    final point = Vector2(
      info.eventPosition.game.x,
      info.eventPosition.game.y,
    );

    if (!containsPoint(point)) return;

    _pressed = true;
    _tapPointers.add(pointerId);
  }

  void onTapUp(int pointerId) {
    if(!_tapPointers.contains(pointerId)) return;

    _pressed = false;

    if (onTap != null) onTap!();

    _tapPointers.remove(pointerId);
  }

  void onTapCancel(int pointerId) {
    if(!_tapPointers.contains(pointerId)) return;

    _pressed = false;
    _tapPointers.remove(pointerId);
  }
}
