import 'package:flame/components.dart';
import 'package:flame_test/controllers/world_size_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Background extends Component {
  static final Paint _paint = Paint()..color = const Color(0xFFF2F2F2);

  final WorldSizeController _worldSizeController = Get.find();

  Vector2 _bgSize = Vector2(0, 0);

  Background(){
    _worldSizeController.subscribe((state) {
      _bgSize = state.worldSize;
    });
  }

  @override
  void render(Canvas canvas) {
    canvas.drawRect(
      Rect.fromLTWH(0, 0, _bgSize.x, _bgSize.y),
      _paint,
    );
    super.render(canvas);
  }
}
