import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flame_test/utils.dart';
import 'package:flutter/material.dart';

class FrameBeam extends PositionComponent with Tappable {
  bool _pressed = false;

  final List<Point<double>> innerPoints;
  final List<Point<double>> outerPoints;

  final String beamGuid;

  FrameBeam({
    required Vector2 position,
    required this.innerPoints,
    required this.outerPoints,
    required Vector2 size,
    required this.beamGuid,
  }) : super(
          position: position,
          size: size,
        );

  factory FrameBeam.fromJson(
    Map<String, dynamic> json,
    Vector2 productPosition,
  ) {
    int xPosition = json['start']['x'];
    int yPosition = json['start']['y'];

    for (var point
        in (json['_outerDrawingPoints'] as List<Map<String, dynamic>>)) {
      xPosition = min(xPosition, point['x']);
      yPosition = min(yPosition, point['y']);
    }

    final Vector2 position = Vector2(
      FlameUtils.asDouble(xPosition),
      FlameUtils.asDouble(yPosition),
    );

    final List<Point<double>> innerPoints = [];
    for (var point
        in (json['_innerDrawingPoints'] as List<Map<String, dynamic>>)) {
      innerPoints.add(Point(
        FlameUtils.asDouble(point['x']) - position.x,
        FlameUtils.asDouble(point['y']) - position.y,
      ));
    }

    final Vector2 size = Vector2.zero();
    final List<Point<double>> outerPoints = [];
    for (var point
        in (json['_outerDrawingPoints'] as List<Map<String, dynamic>>)) {
      final outerPoint = Point(
        FlameUtils.asDouble(point['x']) - position.x,
        FlameUtils.asDouble(point['y']) - position.y,
      );
      outerPoints.add(outerPoint);

      size.x = max(outerPoint.x, size.x);
      size.y = max(outerPoint.y, size.y);
    }

    return FrameBeam(
      position: Vector2(
        position.x - productPosition.x,
        position.y - productPosition.y,
      ),
      innerPoints: innerPoints,
      outerPoints: outerPoints,
      size: size,
      beamGuid: json['beamGuid']
    );
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    _renderPart(canvas, outerPoints);
    _renderPart(canvas, innerPoints);
  }

  void _renderPart(Canvas canvas, List<Point<double>> points) {
    final path = Path();
    final firstPoint = points.first;
    path.moveTo(firstPoint.x, firstPoint.y);
    for (var point in points) {
      path.lineTo(point.x, point.y);
    }
    path.lineTo(firstPoint.x, firstPoint.y);

    canvas.drawPath(
      path,
      _pressed ? pressedFill : fill,
    );

    canvas.drawPath(
      path,
      border,
    );

    // bool contains = false;
    // for (var point in outerPoints) {
    //   if(point.x == 0 && point.y == 0){
    //     contains = true;
    //     break;
    //   }
    // }
    //
    // if(!contains) {
    //   canvas.drawRect(
    //     size.toRect(),
    //     Paint()
    //       ..color = const Color(0xFFD1F8A2)
    //       ..style = PaintingStyle.fill
    //       ..strokeWidth = 1,
    //   );
    // }
  }

  @override
  bool onTapDown(TapDownInfo info) {
    _pressed = !_pressed;
    print(beamGuid);
    return true;
  }

  static final Paint fill = Paint()
    ..color = const Color(0xFFFFFFFF)
    ..style = PaintingStyle.fill
    ..strokeWidth = 1;

  static final Paint pressedFill = Paint()
    ..color = const Color(0xFF30C1FF)
    ..style = PaintingStyle.fill
    ..strokeWidth = 1;

  static final Paint border = Paint()
    ..color = const Color(0xFF000000)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1;
}
