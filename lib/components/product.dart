import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame_test/components/frame_beam.dart';

import '../utils.dart';

class Product extends PositionComponent {
  final List<FrameBeam> frameBeams;

  Product({
    required Vector2 position,
    required this.frameBeams,
    required Vector2 size,
  }) : super(
    position: position,
    size: size,
  );

  factory Product.fromJson(Map<String, dynamic> json,
      Vector2 margin,) {
    final Vector2 position = Vector2(
      FlameUtils.asDouble(json['_refPoint']['x']),
      FlameUtils.asDouble(json['_refPoint']['y']),
    );

    final List<FrameBeam> beams = [];
    for (var beam
    in (json['frame']['frameBeams'] as List<Map<String, dynamic>>)) {
      var frameBeam = FrameBeam.fromJson(beam, position);
      beams.add(frameBeam);
    }

    final Vector2 size = Vector2.zero();
    for (var beam in beams) {
      size.x = max(beam.size.x, size.x);
      size.y = max(beam.size.y, size.y);
    }

    return Product(
      frameBeams: beams,
      position: Vector2(
        position.x + margin.x,
        position.y + margin.y,
      ),
      size: size,
    );
  }

  @override
  Future<void>? onLoad() {
    for (var beam in frameBeams) {
      add(beam);
    }
    flipVerticallyAroundCenter();
    return super.onLoad();
  }
}
