import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class CameraController {
  void subscribeEvents(Camera camera);

  void movePoint(Vector2 position);

  void resize(Vector2 canvasSize);

  void zoomOnChange(double zoom);
}

class CameraControllerImpl implements CameraController {
  final List<Camera> _cameras = [];
  final Rx<Vector2> _moveController = Vector2.zero().obs;
  final Rx<Vector2> _sizeController = Vector2.zero().obs;

  CameraControllerImpl() {
    _moveController.stream.listen((event) {
      for (var element in _cameras) {
        _moveCamera(element, event);
      }
    });

    _sizeController.stream.listen((event) {
      for (var element in _cameras) {
        _resizeCamera(element, event);
      }
    });
  }

  @override
  void subscribeEvents(Camera camera) {
    if (!_cameras.contains(camera)) _cameras.add(camera);
  }

  @override
  void movePoint(Vector2 position) {
    _moveController.value = position;
  }

  @override
  void resize(Vector2 canvasSize) {
    _sizeController.value = canvasSize;
  }

  void _moveCamera(Camera camera, Vector2 position) {
    camera.snapTo(
      Vector2(
        camera.position.x - position.x,
        camera.position.y - position.y,
      ),
    );
  }

  void _resizeCamera(Camera camera, Vector2 size) {
    camera.worldBounds = Rect.fromLTWH(
      0,
      0,
      size.x,
      size.y,
    );
  }

  @override
  void zoomOnChange(double zoom) {
    for (var element in _cameras) {
      element.zoom = zoom;
    }
  }
}
