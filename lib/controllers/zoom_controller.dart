import 'package:flame/game.dart';
import 'package:flame_test/controllers/camera_controller.dart';
import 'package:flame_test/controllers/world_size_controller.dart';
import 'package:get/get.dart';

typedef ZoomOnChange = void Function(double zoom);

abstract class ZoomController {
  void init(double zoom, Vector2 canvasSize);

  void subscribe(ZoomOnChange subscriber);

  void increase();

  void decrease();
}

class ZoomControllerImpl implements ZoomController {
  final RxDouble _zoom = 1.0.obs;

  final List<ZoomOnChange> _subscribers = [];

  final CameraController _cameraController = Get.find();
  final WorldSizeController _worldSizeController = Get.find();

  final Rx<Vector2> _canvasSize = Vector2.zero().obs;

  ZoomControllerImpl() {
    _zoom.listen((value) {
      _cameraController.zoomOnChange(value);
      _worldSizeController.zoomOnChange(value);

      for (var element in _subscribers) {
        element(value);
      }
    });
  }

  @override
  void init(double zoom, Vector2 canvasSize) {
    _zoom.value = zoom;
    _canvasSize.value = canvasSize;
  }

  @override
  void subscribe(ZoomOnChange subscriber) {
    _subscribers.add(subscriber);
  }

  @override
  void increase() {
    double step = _getStep();
    if (_zoom.value < 5) _zoom.value += step;
  }

  @override
  void decrease() {
    double step = _getStep();

    final zoom = _zoom.value - step;
    if (zoom < 0.1) return;

    final canvasSize = _worldSizeController.zoomedCanvas(_zoom.value);
    final constructionsSize = _worldSizeController.constructionsSize();

    if (_canvasBigEnough(canvasSize, constructionsSize)) return;

    _zoom.value = zoom;
  }

  double _getStep() {
    double step = 0.05;
    if(_zoom.value < 0.5) step = 0.01;
    return step;
  }

  bool _canvasBigEnough(Vector2 canvasSize, Vector2 constructionsSize) {
    return canvasSize.x >
            (constructionsSize.x + WorldSizeController.margin * 2) &&
        canvasSize.y > (constructionsSize.y + WorldSizeController.margin * 2);
  }
}
