import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame_test/designer.dart';
import 'package:get/get.dart';

typedef WorldSizeOnChangeListener = void Function(WorldSizeState size);

abstract class WorldSizeController {
  static const margin = Designer.margin;

  void onConstructionsChange(List<PositionComponent> constructions);

  void onGameResize(Vector2 canvasSize);

  void subscribe(WorldSizeOnChangeListener listener);

  void zoomOnChange(double zoom);

  Vector2 zoomedWorldSize(double zoom);

  Vector2 zoomedCanvas(double zoom);

  Vector2 constructionsSize();

  WorldSizeState worldSizeState();
}

class WorldSizeControllerImpl implements WorldSizeController {
  Vector2 _worldSize = Vector2.zero();
  Vector2 _constructionsSize = Vector2.zero();

  final Rx<Vector2> _canvasSize = Vector2.zero().obs;

  final Rx<WorldSizeState> _worldSizeState = WorldSizeState(
    Vector2.zero(),
    Vector2.zero(),
  ).obs;

  final List<WorldSizeOnChangeListener> _subscribers = [];

  final RxList<PositionComponent> _constructions = <PositionComponent>[].obs;

  WorldSizeControllerImpl() {
    _worldSizeState.listen((size) {
      for (var subscriber in _subscribers) {
        subscriber(size);
      }
    });
  }

  @override
  void onConstructionsChange(List<PositionComponent> constructions) {
    _constructions.value = constructions;
    _calcWorldBounds(constructions);
    _stateChanged();
  }

  @override
  void onGameResize(Vector2 canvasSize) {
    _worldSize = canvasSize;
    _canvasSize.value = canvasSize;
    _stateChanged();
  }

  @override
  void subscribe(WorldSizeOnChangeListener listener) {
    _subscribers.add(listener);
    listener(_worldSizeState.value);
  }

  void _calcWorldBounds(List<PositionComponent> constructions) {
    // double x = 0;
    // double y = 0;
    //
    // for (var element in constructions) {
    //   x = max(x, element.position.x + element.size.x + Designer.margin * 2);
    //   y = max(y, element.position.y + element.size.y + Designer.margin * 2);
    // }
    // _constructionsSize = Vector2(x, y);
    //
    // x = max(
    //   x,
    //   _worldSize.x,
    // );
    // y = max(
    //   y,
    //   _worldSize.y,
    // );

    _worldSize = _getWorldSize(constructions);
  }

  Vector2 _getWorldSize(List<PositionComponent> products) {
    double xProd = 0;
    double yProd = 0;
    for (var product in products) {
      xProd = max(product.position.x + product.size.x, xProd);
      yProd = max(product.position.y + product.size.y, yProd);
    }

    final double xCanv = max(
      _canvasSize.value.x,
      xProd + (Designer.margin * 2),
    );
    final double yCanv = max(
      _canvasSize.value.y,
      yProd + (Designer.margin * 2),
    );

    Vector2 worldSizeVector;
    if (xCanv > yCanv) {
      final double ratio = xCanv / _canvasSize.value.x;
      final double other = _canvasSize.value.y * ratio;
      worldSizeVector = Vector2(xCanv, other);
    } else {
      final double ratio = yCanv / _canvasSize.value.y;
      final double other = _canvasSize.value.x * ratio;
      worldSizeVector = Vector2(other, yCanv);
    }

    return worldSizeVector;
  }

  void _stateChanged() {
    _worldSizeState.value = WorldSizeState(
      _worldSize,
      _constructionsSize,
    );
  }

  @override
  void zoomOnChange(double zoom) {
    _worldSize = zoomedWorldSize(zoom);
    _stateChanged();
  }

  @override
  WorldSizeState worldSizeState() => WorldSizeState(
        _worldSizeState.value.worldSize,
        _worldSizeState.value.constructionsSize,
      );

  @override
  Vector2 zoomedWorldSize(double zoom) {
    double x = _canvasSize.value.x;
    double y = _canvasSize.value.y;

    if (zoom < 1) {
      x /= zoom;
      y /= zoom;
    }

    x = max(
      x,
      _constructionsSize.x + WorldSizeController.margin * 2,
    );
    y = max(
      y,
      _constructionsSize.y + WorldSizeController.margin * 2,
    );

    return Vector2(x, y);
  }

  @override
  Vector2 constructionsSize() => Vector2(
        _constructionsSize.x,
        _constructionsSize.y,
      );

  @override
  Vector2 zoomedCanvas(double zoom) {
    double x = _canvasSize.value.x;
    double y = _canvasSize.value.y;

    x /= zoom;
    y /= zoom;

    return Vector2(x, y);
  }
}

class WorldSizeState {
  final Vector2 worldSize;
  final Vector2 constructionsSize;

  WorldSizeState(this.worldSize, this.constructionsSize);
}
