import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flame_test/components/frame_beam.dart';
import 'package:flame_test/components/product.dart';
import 'package:get/get.dart';

typedef ConstructionsOnChangeListener = void Function(
    List<Product> constructions);

typedef TappedElementsOnChangeListener = void Function(
    List<FrameBeam> tappedElements);

abstract class ConstructionController {
  void subscribe(ConstructionsOnChangeListener listener);

  void subscribeTappedElements(TappedElementsOnChangeListener listener);

  void add(Product construction);

  void clear();

  void addLeft(Product construction);

  void addTop(Product construction);

  void onTapDown(int pointerId, TapDownInfo info);

  List<FrameBeam> tappedElements();
}

class ConstructionControllerImpl implements ConstructionController {
  final List<Product> _constructions = [];
  final RxList<Product> _components = <Product>[].obs;

  final RxList<FrameBeam> _tappedElements = <FrameBeam>[].obs;

  final List<ConstructionsOnChangeListener> _subscribers = [];
  final List<TappedElementsOnChangeListener> _tappedElementsSubscribers = [];

  ConstructionControllerImpl() {
    _components.listen((constructions) {
      for (var listener in _subscribers) {
        listener(constructions);
      }
    });

    _tappedElements.listen((tappedElements) {
      for (var listener in _tappedElementsSubscribers) {
        listener(tappedElements);
      }
    });
  }

  @override
  void add(Product construction) {
    _constructions.add(construction);
    _components.value = _constructions.toList();
  }

  @override
  void subscribe(ConstructionsOnChangeListener listener) {
    _subscribers.add(listener);
  }

  @override
  void subscribeTappedElements(TappedElementsOnChangeListener listener) {
    _tappedElementsSubscribers.add(listener);
  }

  @override
  void clear() {
    _constructions.clear();
    _components.value = _constructions.toList();
  }

  @override
  void addLeft(Product construction) {
    for (var element in _constructions) {
      element.position = Vector2(
        element.position.x + construction.size.x,
        element.position.y,
      );
    }
    add(construction);
  }

  @override
  void addTop(Product construction) {
    for (var element in _constructions) {
      element.position = Vector2(
        element.position.x,
        element.position.y + construction.size.y,
      );
    }
    add(construction);
  }

  @override
  void onTapDown(int pointerId, TapDownInfo info) async {
    final List<FrameBeam> tappedElements = [];

    var point = Vector2(
      info.eventPosition.game.x,
      info.eventPosition.game.y,
    );

    for (var construction in _constructions) {
      if (!construction.containsPoint(point)) continue;

      for (var element in construction.children) {
        if (!element.containsPoint(point)) continue;

        (element as Tappable).onTapDown(info);

        tappedElements.add(element as FrameBeam);
      }
    }

    if (tappedElements.isNotEmpty) _tappedElements.value = tappedElements;
  }

  @override
  List<FrameBeam> tappedElements() => _tappedElements.value.toList();
}
