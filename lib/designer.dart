import 'dart:math';

import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame_test/components/add_button.dart';
import 'package:flame_test/components/background.dart';
import 'package:flame_test/components/product.dart';
import 'package:flame_test/controllers/camera_controller.dart';
import 'package:flame_test/controllers/construction_controller.dart';
import 'package:flame_test/controllers/world_size_controller.dart';
import 'package:flame_test/controllers/zoom_controller.dart';
import 'package:flame_test/json/construction.dart';
import 'package:get/get.dart';

class Designer extends FlameGame
    with HasTappableComponents, MultiTouchTapDetector, MultiTouchDragDetector {
  final CameraController _cameraController = Get.find();
  final ConstructionController _constructionController = Get.find();
  final WorldSizeController _worldSizeController = Get.find();
  final ZoomController _zoomController = Get.find();

  static const double margin = 100;

  Vector2 _constructionsSize = Vector2.zero();

  String _currentOverlay = '';

  final List<AddButton> buttons = [
    AddButton(Vector2(0, 0)),
    AddButton(Vector2(0, 0)),
    AddButton(Vector2(0, 0)),
    AddButton(Vector2(0, 0)),
  ];

  Designer() {
    _worldSizeController.subscribe((WorldSizeState state) {
      _cameraController.resize(state.worldSize);
      _constructionsSize = state.constructionsSize;
      // if (_constructionsSize.x > 0) _redrawButtons();
    });

    _constructionController.subscribe((constructions) {
      _worldSizeController.onConstructionsChange(constructions);
      if (constructions.isNotEmpty) add(constructions.last);
    });

    _zoomController.subscribe((zoom) {
      final state = _worldSizeController.worldSizeState();

      camera.followVector2(
        Vector2(
          state.worldSize.x / 2,
          state.worldSize.y / 2,
        ),
        relativeOffset: Anchor.center,
      );
    });
  }

  @override
  void onGameResize(Vector2 canvasSize) {
    _constructionController.clear();
    _cameraController.subscribeEvents(camera);

    _worldSizeController.onGameResize(canvasSize);
    _zoomController.init(camera.zoom, canvasSize);
    super.onGameResize(canvasSize);
  }

  @override
  Future<Function> onLoad() async {
    super.onLoad();

    add(Background());
    // for (var element in buttons) {
    //   add(element);
    // }

    _loadConstructions();

    double prevZoom = camera.zoom + 0.05;
    while (prevZoom > camera.zoom) {
      prevZoom = camera.zoom;
      _zoomController.decrease();
    }

    return () {};
  }

  void _loadConstructions() {
    final List<Product> products = [];
    for (var productJson
        in (construction['products'] as List<Map<String, dynamic>>)) {
      products.add(Product.fromJson(
        productJson,
        Vector2.zero(),
      ));
    }

    double xProd = 0;
    double yProd = 0;
    for (var product in products) {
      xProd = max(product.position.x + product.size.x, xProd);
      yProd = max(product.position.y + product.size.y, yProd);
    }

    final double xCanv = max(canvasSize.x, xProd + (margin * 2));
    final double yCanv = max(canvasSize.y, yProd + (margin * 2));

    final Vector2 marginVector = _getProductsMargin(xCanv, yCanv);
    /*
     TODO: доделать логику
      - вычислить наибольшую сторону
      - увеличить пропорционально вторую сторону канваса
      - для большей стороны отступом будет margin
      - для меньшей - половина экрана (вычисленного) минус половина продукции
     */

    for (var product in products) {
      product.position.x += marginVector.x;
      product.position.y += marginVector.y;
      _constructionController.add(product);
    }
  }

  Vector2 _getProductsMargin(
    double xCanv,
    double yCanv,
  ) {
    Vector2 marginVector;
    if (xCanv > yCanv) {
      final double ratio = xCanv / canvasSize.x;
      final double other = canvasSize.y * ratio;
      marginVector = Vector2(margin, (other - yCanv)/2);
    } else {
      final double ratio = yCanv / canvasSize.y;
      final double other = canvasSize.x * ratio;
      marginVector = Vector2((other - yCanv)/2, margin);
    }
    return marginVector;
  }

  @override
  void onDragUpdate(int pointerId, DragUpdateInfo info) {
    camera.follow = null;
    camera.setRelativeOffset(Anchor.topLeft);

    _cameraController.movePoint(info.delta.global);
  }

  void _redrawButtons() {
    for (var element in buttons) {
      element.removeFromParent();
    }
    buttons.clear();

    buttons.add(AddButton(
      Vector2(
        30,
        margin + (_constructionsSize.y / 2) + (AddButton.side / 2),
      ),
      onTap: () {
        // _constructionController.addLeft(
        //   SimpleConstruction(
        //     Vector2(
        //       margin + AddButton.side,
        //       margin + AddButton.side,
        //     ),
        //   ),
        // );
      },
    ));

    buttons.add(AddButton(
      Vector2(
        margin + AddButton.side / 2 + _constructionsSize.x / 2,
        30,
      ),
      onTap: () {
        // _constructionController.addTop(
        //   SimpleConstruction(
        //     Vector2(
        //       margin + AddButton.side,
        //       margin + AddButton.side,
        //     ),
        //   ),
        // );
      },
    ));

    buttons.add(AddButton(
      Vector2(
        margin + AddButton.side + _constructionsSize.x,
        margin + AddButton.side / 2 + _constructionsSize.y / 2,
      ),
      onTap: () {
        // _constructionController.add(
        //   SimpleConstruction(
        //     Vector2(
        //       margin + AddButton.side + _constructionsSize.x,
        //       margin + AddButton.side,
        //     ),
        //   ),
        // );
      },
    ));

    buttons.add(AddButton(
      Vector2(
        margin + AddButton.side / 2 + _constructionsSize.x / 2,
        margin + AddButton.side + _constructionsSize.y,
      ),
      onTap: () {
        // _constructionController.add(
        //   SimpleConstruction(
        //     Vector2(
        //       margin + AddButton.side,
        //       margin + AddButton.side + _constructionsSize.y,
        //     ),
        //   ),
        // );
      },
    ));

    for (var element in buttons) {
      add(element);
    }
  }

  @override
  void onTapUp(int pointerId, TapUpInfo info) {
    super.onTapUp(pointerId, info);
    for (var button in buttons) {
      button.onTapUp(pointerId);
    }
  }

  @override
  void onTapDown(int pointerId, TapDownInfo info) {
    super.onTapDown(pointerId, info);

    _constructionController.onTapDown(pointerId, info);
    for (var button in buttons) {
      button.onTapDown(pointerId, info);
    }
  }

  @override
  void onTapCancel(int pointerId) {
    super.onTapCancel(pointerId);
    for (var button in buttons) {
      button.onTapCancel(pointerId);
    }
  }

  void showOverlay(String s) {
    _currentOverlay = s;
    overlays.add(s);
  }

  void hideOverlay() {
    overlays.remove(_currentOverlay);
    _currentOverlay = '';
  }
}
