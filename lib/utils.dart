class FlameUtils{
  static double asDouble(dynamic jsonValue) => (jsonValue as int).toDouble();

  static double abs(double val) => val >= 0 ? val : -val;
}