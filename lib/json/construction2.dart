Map<String, dynamic> construction2 = {
    "id": "338688f1-05c1-0ca8-31b7-412422dbf0f4",
    "name": "",
    "totalCost": 0,
    "amegaCost": 0,
    "amegaCostToDisplay": 0,
    "weight": 0,
    "square": 0,
    "quantity": 1,
    "position": 1,
    "products": [
        {
            "productGuid": "99f029c8-c6bb-38aa-9e9a-c91ec0ec3110",
            "frame": {
                "frameFillingGuid": "d9811e0a-390a-78e5-2426-cccbe93a9cf1",
                "shtulpOpenType": 0,
                "frameBeams": [
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 1300,
                            "y": 0
                        },
                        "end": {
                            "x": 0,
                            "y": 0
                        },
                        "beamGuid": "48c9f716-c693-6615-7311-f50a5892b668",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 1300,
                                "y": 0
                            },
                            {
                                "x": 0,
                                "y": 0
                            },
                            {
                                "x": 53,
                                "y": 53
                            },
                            {
                                "x": 1247,
                                "y": 53
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 1300,
                                "y": 0
                            },
                            {
                                "x": 0,
                                "y": 0
                            },
                            {
                                "x": 63,
                                "y": 63
                            },
                            {
                                "x": 1237,
                                "y": 63
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 0,
                            "y": 0
                        },
                        "end": {
                            "x": 0,
                            "y": 1400
                        },
                        "beamGuid": "5f91fbed-9d14-d503-1994-9bfdac28e0b2",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 0,
                                "y": 0
                            },
                            {
                                "x": 0,
                                "y": 1400
                            },
                            {
                                "x": 53,
                                "y": 1347
                            },
                            {
                                "x": 53,
                                "y": 53
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 0,
                                "y": 0
                            },
                            {
                                "x": 0,
                                "y": 1400
                            },
                            {
                                "x": 63,
                                "y": 1337
                            },
                            {
                                "x": 63,
                                "y": 63
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 0,
                            "y": 1400
                        },
                        "end": {
                            "x": 1300,
                            "y": 1400
                        },
                        "beamGuid": "3f1e2940-e47a-c5b5-b1b0-ba7fae3ed06e",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 0,
                                "y": 1400
                            },
                            {
                                "x": 1300,
                                "y": 1400
                            },
                            {
                                "x": 1247,
                                "y": 1347
                            },
                            {
                                "x": 53,
                                "y": 1347
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 0,
                                "y": 1400
                            },
                            {
                                "x": 1300,
                                "y": 1400
                            },
                            {
                                "x": 1237,
                                "y": 1337
                            },
                            {
                                "x": 63,
                                "y": 1337
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 1300,
                            "y": 1400
                        },
                        "end": {
                            "x": 1300,
                            "y": 0
                        },
                        "beamGuid": "a35ddcff-6e78-f0b6-3363-844e91998a17",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 1300,
                                "y": 1400
                            },
                            {
                                "x": 1300,
                                "y": 0
                            },
                            {
                                "x": 1247,
                                "y": 53
                            },
                            {
                                "x": 1247,
                                "y": 1347
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 1300,
                                "y": 1400
                            },
                            {
                                "x": 1300,
                                "y": 0
                            },
                            {
                                "x": 1237,
                                "y": 63
                            },
                            {
                                "x": 1237,
                                "y": 1337
                            }
                        ],
                        "_isConnectionPart": true,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1"
                    }
                ],
                "impostBeams": [
                    {
                        "_width": 88,
                        "_id": null,
                        "start": {
                            "x": 650,
                            "y": 0
                        },
                        "end": {
                            "x": 650,
                            "y": 1400
                        },
                        "beamGuid": "59c592a5-abba-0f18-72ac-5523acc2d987",
                        "modelPart": 3,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 684,
                                "y": 0
                            },
                            {
                                "x": 616,
                                "y": 0
                            },
                            {
                                "x": 616,
                                "y": 1400
                            },
                            {
                                "x": 684,
                                "y": 1400
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 694,
                                "y": 0
                            },
                            {
                                "x": 606,
                                "y": 0
                            },
                            {
                                "x": 606,
                                "y": 1400
                            },
                            {
                                "x": 694,
                                "y": 1400
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "337",
                        "shtulpType": 3,
                        "_dependencyIds": [
                            "48c9f716-c693-6615-7311-f50a5892b668",
                            "3f1e2940-e47a-c5b5-b1b0-ba7fae3ed06e"
                        ],
                        "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1",
                        "_level": 0,
                        "_parentPolygon": {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 0,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 0,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 0,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 0,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                },
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1300,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1300,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1300,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1300,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ]
                        },
                        "_containerPolygon": {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 0,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 0,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 0,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 0,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                },
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1300,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 650,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 650,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1300,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1300,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1300,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ]
                        }
                    }
                ],
                "innerFillings": [
                    {
                        "solid": {
                            "solidFillingGuid": "d7acc978-fd4b-8cbd-f4a2-d05b1b08da28",
                            "userParameters": [
                                {
                                    "id": 1099,
                                    "value": "Нет",
                                    "title": "",
                                    "hex": ""
                                }
                            ],
                            "hardcodedUserParameters": [],
                            "_containerPolygon": {
                                "_id": null,
                                "_segments": [
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 650,
                                            "y": 0
                                        },
                                        "end": {
                                            "x": 0,
                                            "y": 0
                                        }
                                    },
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 0,
                                            "y": 0
                                        },
                                        "end": {
                                            "x": 0,
                                            "y": 1400
                                        }
                                    },
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 0,
                                            "y": 1400
                                        },
                                        "end": {
                                            "x": 650,
                                            "y": 1400
                                        }
                                    },
                                    {
                                        "_width": 34,
                                        "_id": null,
                                        "start": {
                                            "x": 650,
                                            "y": 1400
                                        },
                                        "end": {
                                            "x": 650,
                                            "y": 0
                                        }
                                    }
                                ],
                                "childrenPolygons": []
                            },
                            "filling": {
                                "isDefault": true,
                                "id": 27,
                                "name": "Стеклопакет 32 мм",
                                "marking": "4-10-4-10-4",
                                "thickness": 32,
                                "group": "Стеклопакеты",
                                "camernost": 2,
                                "isMultifunctional": false,
                                "isLowEmission": false
                            },
                            "fillingPoint": {
                                "x": 325,
                                "y": 700
                            },
                            "fillingVendorCode": "4-10-4-10-4",
                            "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1"
                        }
                    },
                    {
                        "leaf": {
                            "frameFillingGuid": "a0344a13-965c-73ed-567b-008a2d7d5385",
                            "shtulpOpenType": 0,
                            "frameBeams": [
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 1265,
                                        "y": 35
                                    },
                                    "end": {
                                        "x": 666,
                                        "y": 35
                                    },
                                    "beamGuid": "88aa5b81-3313-01b7-6cd2-1dbf767834a4",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 1265,
                                            "y": 35
                                        },
                                        {
                                            "x": 666,
                                            "y": 35
                                        },
                                        {
                                            "x": 733,
                                            "y": 102
                                        },
                                        {
                                            "x": 1198,
                                            "y": 102
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 1265,
                                            "y": 35
                                        },
                                        {
                                            "x": 666,
                                            "y": 35
                                        },
                                        {
                                            "x": 743,
                                            "y": 112
                                        },
                                        {
                                            "x": 1188,
                                            "y": 112
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "a0344a13-965c-73ed-567b-008a2d7d5385"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 666,
                                        "y": 35
                                    },
                                    "end": {
                                        "x": 666,
                                        "y": 1365
                                    },
                                    "beamGuid": "106c2c76-ec84-b72e-68f7-152ecf907437",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 666,
                                            "y": 35
                                        },
                                        {
                                            "x": 666,
                                            "y": 1365
                                        },
                                        {
                                            "x": 733,
                                            "y": 1298
                                        },
                                        {
                                            "x": 733,
                                            "y": 102
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 666,
                                            "y": 35
                                        },
                                        {
                                            "x": 666,
                                            "y": 1365
                                        },
                                        {
                                            "x": 743,
                                            "y": 1288
                                        },
                                        {
                                            "x": 743,
                                            "y": 112
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "a0344a13-965c-73ed-567b-008a2d7d5385"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 666,
                                        "y": 1365
                                    },
                                    "end": {
                                        "x": 1265,
                                        "y": 1365
                                    },
                                    "beamGuid": "84262636-7790-b32d-8c48-5de579027aa5",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 666,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1265,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1198,
                                            "y": 1298
                                        },
                                        {
                                            "x": 733,
                                            "y": 1298
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 666,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1265,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1188,
                                            "y": 1288
                                        },
                                        {
                                            "x": 743,
                                            "y": 1288
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "a0344a13-965c-73ed-567b-008a2d7d5385"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 1265,
                                        "y": 1365
                                    },
                                    "end": {
                                        "x": 1265,
                                        "y": 35
                                    },
                                    "beamGuid": "a7557336-9b74-4962-70b2-3d8ecc4a560b",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 1265,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1265,
                                            "y": 35
                                        },
                                        {
                                            "x": 1198,
                                            "y": 102
                                        },
                                        {
                                            "x": 1198,
                                            "y": 1298
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 1265,
                                            "y": 1365
                                        },
                                        {
                                            "x": 1265,
                                            "y": 35
                                        },
                                        {
                                            "x": 1188,
                                            "y": 112
                                        },
                                        {
                                            "x": 1188,
                                            "y": 1288
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "a0344a13-965c-73ed-567b-008a2d7d5385"
                                }
                            ],
                            "impostBeams": [],
                            "innerFillings": [
                                {
                                    "solid": {
                                        "solidFillingGuid": "d2ea88ea-9d3a-7c10-4b78-2e067e545d90",
                                        "userParameters": [
                                            {
                                                "id": 1099,
                                                "value": "Нет",
                                                "title": "",
                                                "hex": ""
                                            },
                                            {
                                                "id": 2698,
                                                "value": "Нет",
                                                "title": "Вклейка стеклопакета",
                                                "hex": ""
                                            }
                                        ],
                                        "hardcodedUserParameters": [],
                                        "_containerPolygon": {
                                            "_id": null,
                                            "_segments": [
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 1265,
                                                        "y": 35
                                                    },
                                                    "end": {
                                                        "x": 666,
                                                        "y": 35
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 666,
                                                        "y": 35
                                                    },
                                                    "end": {
                                                        "x": 666,
                                                        "y": 1365
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 666,
                                                        "y": 1365
                                                    },
                                                    "end": {
                                                        "x": 1265,
                                                        "y": 1365
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 1265,
                                                        "y": 1365
                                                    },
                                                    "end": {
                                                        "x": 1265,
                                                        "y": 35
                                                    }
                                                }
                                            ],
                                            "childrenPolygons": []
                                        },
                                        "filling": {
                                            "isDefault": true,
                                            "id": 27,
                                            "name": "Стеклопакет 32 мм",
                                            "marking": "4-10-4-10-4",
                                            "thickness": 32,
                                            "group": "Стеклопакеты",
                                            "camernost": 2,
                                            "isMultifunctional": false,
                                            "isLowEmission": false
                                        },
                                        "fillingPoint": {
                                            "x": 965.5,
                                            "y": 700
                                        },
                                        "fillingVendorCode": "4-10-4-10-4",
                                        "_parentNodeId": "a0344a13-965c-73ed-567b-008a2d7d5385"
                                    }
                                }
                            ],
                            "moskitka": {
                                "set": false,
                                "color": "white",
                                "catproof": false
                            },
                            "handlePositionType": "center",
                            "handleColor": "Белый",
                            "userParameters": [
                                {
                                    "id": 413,
                                    "value": "Обычные",
                                    "title": "Петли оконные",
                                    "hex": ""
                                },
                                {
                                    "id": 416,
                                    "value": "Авто",
                                    "title": "Средний запор для Масо",
                                    "hex": ""
                                }
                            ],
                            "hardcodedUserParameters": [
                                {
                                    "id": 408,
                                    "value": "5-ти ступенчатое",
                                    "title": "Многоступенчатое микропроветривание",
                                    "hex": ""
                                }
                            ],
                            "_parentNodeId": "d9811e0a-390a-78e5-2426-cccbe93a9cf1",
                            "_containerPolygon": {
                                "_id": null,
                                "_segments": [
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 1265,
                                            "y": 35
                                        },
                                        "end": {
                                            "x": 666,
                                            "y": 35
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 666,
                                            "y": 35
                                        },
                                        "end": {
                                            "x": 666,
                                            "y": 1365
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 666,
                                            "y": 1365
                                        },
                                        "end": {
                                            "x": 1265,
                                            "y": 1365
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 1265,
                                            "y": 1365
                                        },
                                        "end": {
                                            "x": 1265,
                                            "y": 35
                                        }
                                    }
                                ],
                                "childrenPolygons": []
                            },
                            "_fillingPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 1265,
                                                "y": 35
                                            },
                                            "end": {
                                                "x": 666,
                                                "y": 35
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 666,
                                                "y": 35
                                            },
                                            "end": {
                                                "x": 666,
                                                "y": 1365
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 666,
                                                "y": 1365
                                            },
                                            "end": {
                                                "x": 1265,
                                                "y": 1365
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 1265,
                                                "y": 1365
                                            },
                                            "end": {
                                                "x": 1265,
                                                "y": 35
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ],
                            "_isMoskitkaRoot": false,
                            "_isMoskitka": false,
                            "_isEmpty": false,
                            "_isEntranceDoor": false,
                            "_isAluminium": false,
                            "_isMetalDoor": false,
                            "fillingPoint": {
                                "x": 965.5,
                                "y": 700
                            },
                            "furniture": {
                                "id": 27,
                                "name": "Фурнитура Maco MM",
                                "displayName": "Maco MM",
                                "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                                "isDefault": false,
                                "isEmpty": false
                            },
                            "handleType": "МакБет",
                            "openSide": 1,
                            "openType": 2,
                            "profile": {
                                "allowedThickness": [
                                    0,
                                    4,
                                    5,
                                    6,
                                    24,
                                    28,
                                    32
                                ],
                                "parts": [
                                    {
                                        "id": 287,
                                        "marking": "PR SP 751",
                                        "modelPart": 8,
                                        "a": 26,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Фальш-переплет на белой основе"
                                    },
                                    {
                                        "id": 310,
                                        "marking": "807",
                                        "modelPart": 1,
                                        "a": 63,
                                        "b": 0,
                                        "c": 43,
                                        "d": 0,
                                        "e": null,
                                        "f": 0,
                                        "g": 0,
                                        "comment": "Оконная рама"
                                    },
                                    {
                                        "id": 308,
                                        "marking": "817_04",
                                        "modelPart": 2,
                                        "a": 77,
                                        "b": 0,
                                        "c": 57,
                                        "d": 0,
                                        "e": 20,
                                        "f": 0,
                                        "g": 0,
                                        "comment": "Створка оконная 77мм"
                                    },
                                    {
                                        "id": 290,
                                        "marking": "150",
                                        "modelPart": 4,
                                        "a": 5,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель Н-образный"
                                    },
                                    {
                                        "id": 298,
                                        "marking": "337",
                                        "modelPart": 3,
                                        "a": 44,
                                        "b": null,
                                        "c": 24,
                                        "d": 0,
                                        "e": null,
                                        "f": 0,
                                        "g": 6,
                                        "comment": "Импост оконный"
                                    },
                                    {
                                        "id": 299,
                                        "marking": "734",
                                        "modelPart": 6,
                                        "a": 32,
                                        "b": null,
                                        "c": 12,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Штульп оконный"
                                    },
                                    {
                                        "id": 291,
                                        "marking": "152",
                                        "modelPart": 4,
                                        "a": 20,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель Н-образный с армированием"
                                    },
                                    {
                                        "id": 292,
                                        "marking": "155",
                                        "modelPart": 4,
                                        "a": 149,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель угловой 90 градусов"
                                    },
                                    {
                                        "id": 553,
                                        "marking": "540+541",
                                        "modelPart": 4,
                                        "a": 182,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель с переменным углом"
                                    },
                                    {
                                        "id": 296,
                                        "marking": "144",
                                        "modelPart": 4,
                                        "a": 30,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 30 мм"
                                    },
                                    {
                                        "id": 295,
                                        "marking": "545",
                                        "modelPart": 4,
                                        "a": 45,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 45 мм"
                                    },
                                    {
                                        "id": 294,
                                        "marking": "546",
                                        "modelPart": 4,
                                        "a": 60,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 60 мм"
                                    },
                                    {
                                        "id": 293,
                                        "marking": "147",
                                        "modelPart": 4,
                                        "a": 120,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 120 мм"
                                    },
                                    {
                                        "id": 754,
                                        "marking": "Соединитель не определен",
                                        "modelPart": 4,
                                        "a": 50,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель не определен"
                                    }
                                ],
                                "class": "Б",
                                "brand": "KBE",
                                "camernost": 3,
                                "thickness": 58,
                                "isDefault": false,
                                "isEmpty": false,
                                "isMoskitka": false,
                                "isEnergy": null,
                                "id": 16,
                                "name": "KBE Эталон 58",
                                "displayName": "КБЕ 58мм"
                            }
                        }
                    }
                ],
                "moskitka": {
                    "set": false,
                    "color": "white",
                    "catproof": false
                },
                "handlePositionType": "center",
                "handleColor": "Белый",
                "userParameters": [],
                "hardcodedUserParameters": [],
                "_parentNodeId": null,
                "_containerPolygon": {
                    "_id": null,
                    "_segments": [
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 1300,
                                "y": 0
                            },
                            "end": {
                                "x": 0,
                                "y": 0
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 0,
                                "y": 0
                            },
                            "end": {
                                "x": 0,
                                "y": 1400
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 0,
                                "y": 1400
                            },
                            "end": {
                                "x": 1300,
                                "y": 1400
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 1300,
                                "y": 1400
                            },
                            "end": {
                                "x": 1300,
                                "y": 0
                            }
                        }
                    ],
                    "childrenPolygons": [
                        {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 650,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 0,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 0,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 650,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 34,
                                    "_id": null,
                                    "start": {
                                        "x": 650,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 650,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": []
                        },
                        {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 650,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 34,
                                    "_id": null,
                                    "start": {
                                        "x": 650,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 650,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 650,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1300,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1300,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": []
                        }
                    ]
                },
                "_fillingPolygons": [
                    {
                        "_id": null,
                        "_segments": [
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 650,
                                    "y": 0
                                },
                                "end": {
                                    "x": 0,
                                    "y": 0
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 0,
                                    "y": 0
                                },
                                "end": {
                                    "x": 0,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 0,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 650,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 34,
                                "_id": null,
                                "start": {
                                    "x": 650,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 650,
                                    "y": 0
                                }
                            }
                        ],
                        "childrenPolygons": []
                    },
                    {
                        "_id": null,
                        "_segments": [
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1300,
                                    "y": 0
                                },
                                "end": {
                                    "x": 650,
                                    "y": 0
                                }
                            },
                            {
                                "_width": 34,
                                "_id": null,
                                "start": {
                                    "x": 650,
                                    "y": 0
                                },
                                "end": {
                                    "x": 650,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 650,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 1300,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1300,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 1300,
                                    "y": 0
                                }
                            }
                        ],
                        "childrenPolygons": []
                    }
                ],
                "_isMoskitkaRoot": false,
                "_isMoskitka": false,
                "_isEmpty": false,
                "_isEntranceDoor": false,
                "_isAluminium": false,
                "_isMetalDoor": false,
                "fillingPoint": {
                    "x": 650,
                    "y": 700
                },
                "furniture": {
                    "id": 27,
                    "name": "Фурнитура Maco MM",
                    "displayName": "Maco MM",
                    "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                    "isDefault": false,
                    "isEmpty": false
                },
                "handleType": "МакБет",
                "openSide": 4,
                "openType": 0,
                "profile": {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        28,
                        32
                    ],
                    "parts": [
                        {
                            "id": 287,
                            "marking": "PR SP 751",
                            "modelPart": 8,
                            "a": 26,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Фальш-переплет на белой основе"
                        },
                        {
                            "id": 310,
                            "marking": "807",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Оконная рама"
                        },
                        {
                            "id": 308,
                            "marking": "817_04",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная 77мм"
                        },
                        {
                            "id": 290,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 298,
                            "marking": "337",
                            "modelPart": 3,
                            "a": 44,
                            "b": null,
                            "c": 24,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 299,
                            "marking": "734",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 291,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 292,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 553,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 296,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 295,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 294,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 293,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 754,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "KBE",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 16,
                    "name": "KBE Эталон 58",
                    "displayName": "КБЕ 58мм"
                }
            },
            "connectors": [
                {
                    "_width": 5,
                    "_id": null,
                    "start": {
                        "x": 1300,
                        "y": 0
                    },
                    "end": {
                        "x": 1300,
                        "y": 1400
                    },
                    "beamGuid": "63389da8-ca0e-e0cd-1bdc-209b61dcb097",
                    "modelPart": 4,
                    "userParameters": [
                        {
                            "id": 340,
                            "value": "Авто",
                            "title": "Армирование",
                            "hex": ""
                        }
                    ],
                    "hardcodedUserParameters": [],
                    "_innerDrawingPoints": [
                        {
                            "x": 1300,
                            "y": 0
                        },
                        {
                            "x": 1305,
                            "y": 0
                        },
                        {
                            "x": 1305,
                            "y": 1400
                        },
                        {
                            "x": 1300,
                            "y": 1400
                        }
                    ],
                    "_outerDrawingPoints": [],
                    "_isConnectionPart": false,
                    "_hasInnerWidth": false,
                    "profileVendorCode": "150",
                    "isCustom": false,
                    "_connectorType": "connector",
                    "connectorOffset": 0,
                    "connectedBeamGuid": "a35ddcff-6e78-f0b6-3363-844e91998a17",
                    "connectedProductGuid": "b5a9569a-1d69-4091-0ac9-b5044d5c059d",
                    "connectedProductBeamGuid": "a35ddcff-6e78-f0b6-3363-844e91998a17",
                    "_parentNodeId": "99f029c8-c6bb-38aa-9e9a-c91ec0ec3110"
                }
            ],
            "heatTransferResistanceCoef": 0,
            "productOffset": 0,
            "userParameters": [
                {
                    "id": 822,
                    "value": "Авто",
                    "title": "Толщина армирования",
                    "hex": ""
                }
            ],
            "hardcodedUserParameters": [
                {
                    "id": 1878,
                    "value": "Амега Пермь",
                    "title": "Бренд",
                    "hex": ""
                },
                {
                    "id": 2831,
                    "value": "Металлическое",
                    "title": "Крепление импоста ПВХ",
                    "hex": ""
                }
            ],
            "_refPoint": {
                "x": 0,
                "y": 0
            },
            "_availableProfiles": [
                {
                    "allowedThickness": [
                        0,
                        1,
                        4,
                        5,
                        6,
                        24,
                        32,
                        36
                    ],
                    "parts": [
                        {
                            "id": 222,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 4,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 243,
                            "marking": "S358.19",
                            "modelPart": 6,
                            "a": 32,
                            "b": 0,
                            "c": 32,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 251,
                            "marking": "S358.02",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 253,
                            "marking": "S358.01",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": null,
                            "comment": "Рама оконная 63 мм"
                        },
                        {
                            "id": 257,
                            "marking": "S358.03",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": 0,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 224,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 26,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": 10,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 225,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": 58,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 239,
                            "marking": "#S-358.02",
                            "modelPart": 1,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама(створка) 77 мм"
                        },
                        {
                            "id": 220,
                            "marking": "#S-358.03",
                            "modelPart": 1,
                            "a": 82,
                            "b": null,
                            "c": 62,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама(импост) 82 мм"
                        },
                        {
                            "id": 223,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 95,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 935,
                            "marking": "#S358-01",
                            "modelPart": 3,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Импост из рамы для кабинок"
                        },
                        {
                            "id": 229,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 228,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": 45,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 227,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 60,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 226,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 120,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 250,
                            "marking": "S-358.26",
                            "modelPart": 2,
                            "a": 98,
                            "b": 0,
                            "c": 78,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": null,
                            "comment": "Створка дверная Z98"
                        },
                        {
                            "id": 756,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        },
                        {
                            "id": 930,
                            "marking": "S358.23",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": 58,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "соединитель 358.23 (90 гр. Экспроф)"
                        }
                    ],
                    "class": "А",
                    "brand": "Exprof",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 14,
                    "name": "Exprof Practica 58mm",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        32,
                        40,
                        42,
                        44
                    ],
                    "parts": [
                        {
                            "id": 204,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 583,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 216,
                            "marking": "S571.11",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Коробка 63 мм"
                        },
                        {
                            "id": 219,
                            "marking": "S571.13",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        },
                        {
                            "id": 1078,
                            "marking": "S670.19",
                            "modelPart": 6,
                            "a": 20,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп для S670.19"
                        },
                        {
                            "id": 942,
                            "marking": "S571.22",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 205,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 582,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 203,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": 584,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 850,
                            "marking": "S571.23",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост усиленный"
                        },
                        {
                            "id": 472,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 100,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 473,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 202,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 589,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 643,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 590,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 760,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Exprof",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 13,
                    "name": "Exprof Profecta 70mm",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        28,
                        32
                    ],
                    "parts": [
                        {
                            "id": 287,
                            "marking": "PR SP 751",
                            "modelPart": 8,
                            "a": 26,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Фальш-переплет на белой основе"
                        },
                        {
                            "id": 310,
                            "marking": "807",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Оконная рама"
                        },
                        {
                            "id": 308,
                            "marking": "817_04",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная 77мм"
                        },
                        {
                            "id": 290,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 298,
                            "marking": "337",
                            "modelPart": 3,
                            "a": 44,
                            "b": null,
                            "c": 24,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 299,
                            "marking": "734",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 291,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 292,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 553,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 296,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 295,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 294,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 293,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 754,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "KBE",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 16,
                    "name": "KBE Эталон 58",
                    "displayName": "КБЕ 58мм"
                },
                {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 856,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с Н-образный"
                        },
                        {
                            "id": 350,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 351,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30"
                        },
                        {
                            "id": 352,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 354,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 417,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 644,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 764,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        },
                        {
                            "id": 353,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с Н-образный с армированием"
                        },
                        {
                            "id": 349,
                            "marking": "PW 20104",
                            "modelPart": 4,
                            "a": 2,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 343,
                            "marking": "PW-0707 PW",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама 63 мм"
                        },
                        {
                            "id": 344,
                            "marking": "PW-0317 PW",
                            "modelPart": 2,
                            "a": 76,
                            "b": 0,
                            "c": 56,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка 77 мм"
                        },
                        {
                            "id": 345,
                            "marking": "PW-0132 PW",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        }
                    ],
                    "class": "",
                    "brand": "Plaswin",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": true,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 18,
                    "name": "Plaswin",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        40
                    ],
                    "parts": [
                        {
                            "id": 264,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 270,
                            "marking": "385.06",
                            "modelPart": 6,
                            "a": 32,
                            "b": 0,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 280,
                            "marking": "395",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 282,
                            "marking": "390",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 286,
                            "marking": "392",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 281,
                            "marking": "70102",
                            "modelPart": 1,
                            "a": 69,
                            "b": 0,
                            "c": 49,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Рама дверная"
                        },
                        {
                            "id": 259,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 258,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель уговой 90 градусов"
                        },
                        {
                            "id": 2571,
                            "marking": "#392",
                            "modelPart": 1,
                            "a": 82,
                            "b": null,
                            "c": 62,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама из импоста"
                        },
                        {
                            "id": 265,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 262,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 261,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 260,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 759,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "А",
                    "brand": "KBE",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 15,
                    "name": "KBE Эксперт 70",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        1,
                        4,
                        5,
                        6,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 475,
                            "marking": "PR 1.063_T",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 476,
                            "marking": "PR 2.077_T",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 477,
                            "marking": "PR 3.082_T",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 478,
                            "marking": "PR3.065T",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 32,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 482,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 483,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 485,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 487,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 490,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 492,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45"
                        },
                        {
                            "id": 494,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 493,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60"
                        },
                        {
                            "id": 933,
                            "marking": "S358.23",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "соединитель 358.23 (90 гр. Экспроф)"
                        },
                        {
                            "id": 768,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Proplex",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 25,
                    "name": "Proplex Litex",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 816,
                            "marking": "555 039",
                            "modelPart": 3,
                            "a": 38,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 0,
                            "comment": ""
                        },
                        {
                            "id": 817,
                            "marking": "#554 230",
                            "modelPart": 6,
                            "a": 31,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 819,
                            "marking": "639 571",
                            "modelPart": 4,
                            "a": 3,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель рамный 3/60"
                        },
                        {
                            "id": 814,
                            "marking": "555 009",
                            "modelPart": 1,
                            "a": 56,
                            "b": null,
                            "c": 38,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 815,
                            "marking": "555 029",
                            "modelPart": 2,
                            "a": 75,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 826,
                            "marking": "560 014 01",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный усиленный"
                        },
                        {
                            "id": 821,
                            "marking": "560 290 01",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90"
                        },
                        {
                            "id": 822,
                            "marking": "140 551 01 + 621 082 01",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Круглый соединитель с ответной частью"
                        },
                        {
                            "id": 824,
                            "marking": "561 004 01",
                            "modelPart": 4,
                            "a": 40,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 40 мм"
                        },
                        {
                            "id": 823,
                            "marking": "561 433 01",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 825,
                            "marking": "561 203 01",
                            "modelPart": 4,
                            "a": 100,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 100 мм"
                        },
                        {
                            "id": 820,
                            "marking": "732 460",
                            "modelPart": 4,
                            "a": 2,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 827,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Rehau",
                    "camernost": 3,
                    "thickness": 60,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 40,
                    "name": "Rehau Blitz New",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        36,
                        40
                    ],
                    "parts": [
                        {
                            "id": 833,
                            "marking": "PW70-3901",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 835,
                            "marking": "PW70-3951",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 837,
                            "marking": "PW70-3921",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 838,
                            "marking": "385",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 841,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 840,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 842,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 839,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 844,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 845,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 846,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 847,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "",
                    "brand": "Plaswin",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 41,
                    "name": "Plaswin 70",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        40
                    ],
                    "parts": [
                        {
                            "id": 877,
                            "marking": "546 301",
                            "modelPart": 4,
                            "a": 3,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель рамный 3/70"
                        },
                        {
                            "id": 878,
                            "marking": "560 024",
                            "modelPart": 4,
                            "a": 18,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Усилитель H-образный"
                        },
                        {
                            "id": 875,
                            "marking": "550 535",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп 64мм"
                        },
                        {
                            "id": 873,
                            "marking": "505 600",
                            "modelPart": 3,
                            "a": 38,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 0,
                            "comment": "Импост 76мм"
                        },
                        {
                            "id": 870,
                            "marking": "505 510",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 45,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама 63мм"
                        },
                        {
                            "id": 871,
                            "marking": "505 700",
                            "modelPart": 2,
                            "a": 75,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка Z 55мм"
                        },
                        {
                            "id": 879,
                            "marking": "561 133",
                            "modelPart": 4,
                            "a": 190,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Угловой соединитель 90 град"
                        },
                        {
                            "id": 880,
                            "marking": "561 166 + 561 176",
                            "modelPart": 4,
                            "a": 186,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Круглый соединитель с ответной частью"
                        },
                        {
                            "id": 2577,
                            "marking": "560 303",
                            "modelPart": 4,
                            "a": 40,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 40мм"
                        },
                        {
                            "id": 882,
                            "marking": "550 130",
                            "modelPart": 4,
                            "a": 100,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 100мм"
                        },
                        {
                            "id": 883,
                            "marking": "561 116",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60мм"
                        }
                    ],
                    "class": "А",
                    "brand": "Rehau",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 43,
                    "name": "Rehau Grazio",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        40,
                        42,
                        48
                    ],
                    "parts": [
                        {
                            "id": 1023,
                            "marking": "76105.07",
                            "modelPart": 1,
                            "a": 67,
                            "b": null,
                            "c": 46,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 1026,
                            "marking": "76304.77",
                            "modelPart": 3,
                            "a": 42,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 1027,
                            "marking": "76402.06",
                            "modelPart": 6,
                            "a": 33,
                            "b": null,
                            "c": 11,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 1029,
                            "marking": "76604",
                            "modelPart": 4,
                            "a": 6,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 1030,
                            "marking": "76605",
                            "modelPart": 4,
                            "a": 22,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 1031,
                            "marking": "8355+76821",
                            "modelPart": 4,
                            "a": 192,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 1032,
                            "marking": "8340+8341+76821",
                            "modelPart": 4,
                            "a": 208,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 1033,
                            "marking": "76701",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 1034,
                            "marking": "76702",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 1035,
                            "marking": "76703",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 2153,
                            "marking": "76303",
                            "modelPart": -3,
                            "a": 55,
                            "b": null,
                            "c": 34,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 1025,
                            "marking": "76209.67",
                            "modelPart": 2,
                            "a": 78,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        }
                    ],
                    "class": "Б",
                    "brand": "KÖMMERLING",
                    "camernost": 5,
                    "thickness": 76,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 46,
                    "name": "KBE 76",
                    "displayName": "Kömmerling 76"
                },
                {
                    "allowedThickness": [
                        0,
                        32,
                        40,
                        42,
                        44
                    ],
                    "parts": [
                        {
                            "id": 2599,
                            "marking": "S670.03",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        },
                        {
                            "id": 2204,
                            "marking": "S571.23",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост усиленный"
                        },
                        {
                            "id": 2220,
                            "marking": "S670.19",
                            "modelPart": 6,
                            "a": 20,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп для S670.19"
                        },
                        {
                            "id": 2206,
                            "marking": "S670.01",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Коробка 63 мм"
                        },
                        {
                            "id": 2209,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 583,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 2215,
                            "marking": "S670.02",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 2210,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 582,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 2208,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": 584,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 2211,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 100,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 2212,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 2207,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 589,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 2213,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 590,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 2214,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "",
                    "brand": "Exprof",
                    "camernost": 6,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 98,
                    "name": "Exprof Experta 70mm",
                    "displayName": ""
                }
            ],
            "_availableFurniture": [
                {
                    "id": 1,
                    "name": "Фурнитура Roto NT",
                    "displayName": "Roto NT",
                    "vars": "140,280;190,481;283,601;433,801;533,1001;583,1201;1020,1801",
                    "isDefault": false,
                    "isEmpty": false
                },
                {
                    "id": 33,
                    "name": "Фурнитура Axor",
                    "displayName": "Axor",
                    "vars": "195,400;220,501;320,681;470,921;620,1161;1020,1881",
                    "isDefault": true,
                    "isEmpty": false
                },
                {
                    "id": 27,
                    "name": "Фурнитура Maco MM",
                    "displayName": "Maco MM",
                    "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                    "isDefault": false,
                    "isEmpty": false
                }
            ],
            "_availableFillings": [
                {
                    "isDefault": false,
                    "id": 89,
                    "name": "Стекло 4 мм Матовое",
                    "marking": "4 W",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 303,
                    "name": "4мм Stemalit RAL8017",
                    "marking": "4мм Stemalit RAL8017",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 63,
                    "name": "Зеркало 4 мм",
                    "marking": "4мм Зеркало",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 73,
                    "name": "Стекло 4 мм М1",
                    "marking": "4мм Стекло",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 76,
                    "name": "стекло 4 мм М1 Solar",
                    "marking": "ClimaGuard Solar 4.00 мм",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 121,
                    "name": "Стекло Комфорт серебристо-серый 40, 4мм",
                    "marking": "Comfort Silver Gr 40, 4мм",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 75,
                    "name": "стекло 4 мм М1 Bronze",
                    "marking": "SunGuard HP Light Bronze",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 19,
                    "name": "SunGuard HP Royal Blue 38",
                    "marking": "SunGuard HP Royal Blue 38",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 74,
                    "name": "стекло 4 мм M1 Silver",
                    "marking": "SunGuard HP Silver 35/26",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 5,
                    "name": "Без заполнения 4 мм",
                    "marking": "Без заполнения 4 мм",
                    "thickness": 4,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 262,
                    "name": "Вентрешетка 4мм Тендер (ФС)",
                    "marking": "Вентрешетка 4мм Тендер (ФС)",
                    "thickness": 4,
                    "group": "Непрозрачное заполнение",
                    "camernost": 0,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 20,
                    "name": "Стекло 5 мм",
                    "marking": "5мм Стекло",
                    "thickness": 5,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 7,
                    "name": "Без заполнения 5 мм",
                    "marking": "Без заполнения 5 мм",
                    "thickness": 5,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 116,
                    "name": "Стекло 6 мм Тонированное в массе",
                    "marking": "6 Т",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 161,
                    "name": "Стекло 6 мм RAL 9003",
                    "marking": "6 мм RAL 9003",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 92,
                    "name": "стекло 6 мм закаленное PlanibelGrey",
                    "marking": "6зак PlanibelGrey",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 194,
                    "name": "стекло 6 мм ST Ph Bronze зак. шлиф.",
                    "marking": "6зак ST Ph Bronze",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 159,
                    "name": "стекло 6 мм СМ3 Голубое 20 Зак шлиф (штамп)",
                    "marking": "6зак ST blue Vision 50T",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 105,
                    "name": "6зак SunGuard HD Black",
                    "marking": "6зак SunGuard HD Black",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 104,
                    "name": "6зак SunGuard HD Grey",
                    "marking": "6зак SunGuard HD Grey",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 141,
                    "name": "стекло 6 мм СМ3 Голубое 20 Зак шлиф (штамп)",
                    "marking": "6зак Голубое СМ3",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 44,
                    "name": "Стекло 6 мм Triplex",
                    "marking": "6мм Triplex",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 143,
                    "name": "стекло 6 мм СМ3 Голубое 20",
                    "marking": "6мм Голубое СМ3",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 21,
                    "name": "Стекло 6 мм",
                    "marking": "6мм Стекло",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 168,
                    "name": "Cтекло 6 мм Matelux clear зак.",
                    "marking": "6мм зак Matelux clear",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 78,
                    "name": "стекло 6 мм М1 Solar",
                    "marking": "ClimaGuard Solar 6.00 мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 91,
                    "name": "SunGuard HD blue 6 мм",
                    "marking": "SunGuard HD blue 6 мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 77,
                    "name": "стекло 6 мм М1 Bronze",
                    "marking": "SunGuard HP Light Bronze6",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 8,
                    "name": "Без заполнения 6 мм",
                    "marking": "Без заполнения 6 мм",
                    "thickness": 6,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 122,
                    "name": "стекло Комфорт голубой 40, 6мм",
                    "marking": "Комфорт голубой 40, 6мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 57,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4 Ц-16-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 252,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4 Ц-16-4 i",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 119,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 54,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6 Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 280,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 6,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 55,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4 i",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 56,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4 Т",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 305,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16Ar-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 106,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-6-4-6-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 132,
                    "name": "Стеклопакет 24 мм",
                    "marking": "5-14-5",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 99,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6 Т[A1]-12Ar-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 250,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-10-8мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 66,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-12-6",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 278,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-12-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 128,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-14-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 214,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6мм Triplex-12-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 11,
                    "name": "Без заполнения 24 мм",
                    "marking": "Без заполнения 24 мм",
                    "thickness": 24,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 85,
                    "name": "Пенополистирол 24 мм с оцинковкой",
                    "marking": "Пенополистирол 24 мм с оцинковкой",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 1,
                    "name": "Сэндвич 24 мм",
                    "marking": "Сэндвич 24 мм",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 94,
                    "name": "Сэндвич 24 мм",
                    "marking": "Сэндвич 24 мм с оцинковкой с внутр стороны",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 231,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4 Ц-8-4-8-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 109,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-20-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 283,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-6-4-10-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 139,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-8-4-8-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 215,
                    "name": "Стеклопакет 28 мм",
                    "marking": "6-16-6",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 47,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4 Ц-10-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": true,
                    "id": 27,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 43,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 46,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4 Т",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 107,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-8-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 48,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10Ar-4-10Ar-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 123,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-12-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 38,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-24-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 59,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-24-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 289,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-6-4-14-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 142,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-8-4-10-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 40,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-8-4-12-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 145,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-9-4-9-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 241,
                    "name": "Стеклопакет 32 мм",
                    "marking": "5-8-4-10-5",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 229,
                    "name": "Стеклопакет 32 мм",
                    "marking": "5-9-4-9-5",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 69,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6 Ц-8-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 144,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-10-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 180,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-10-4-8-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 130,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-20-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 222,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-20-6 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 120,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 134,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-4-8-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 275,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-6-6-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 163,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-6-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 164,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-9-4-9-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 71,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6hard-20-6 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 238,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6мм Triplex-20-6мм Triplex",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 258,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8зак-8-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 223,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8мм Triplex-8-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 224,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8мм Triplex-8-4-8-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 12,
                    "name": "Без заполнения 32 мм",
                    "marking": "Без заполнения 32 мм",
                    "thickness": 32,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 4,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 95,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм с оцинковкой с внеш стороны",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 96,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм с оцинковкой с внутр стороны",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                }
            ],
            "constructionTypeId": 2,
            "furniture": {
                "id": 27,
                "name": "Фурнитура Maco MM",
                "displayName": "Maco MM",
                "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                "isDefault": false,
                "isEmpty": false
            },
            "hardwareSystemName": "Фурнитура Maco MM",
            "insideColorId": 2,
            "insideLamination": {
                "id": 2,
                "name": "Белый",
                "hex": "#fff"
            },
            "outsideColorId": 2,
            "outsideLamination": {
                "id": 2,
                "name": "Белый",
                "hex": "#fff"
            },
            "productionTypeId": 898,
            "profile": {
                "allowedThickness": [
                    0,
                    4,
                    5,
                    6,
                    24,
                    28,
                    32
                ],
                "parts": [
                    {
                        "id": 287,
                        "marking": "PR SP 751",
                        "modelPart": 8,
                        "a": 26,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Фальш-переплет на белой основе"
                    },
                    {
                        "id": 310,
                        "marking": "807",
                        "modelPart": 1,
                        "a": 63,
                        "b": 0,
                        "c": 43,
                        "d": 0,
                        "e": null,
                        "f": 0,
                        "g": 0,
                        "comment": "Оконная рама"
                    },
                    {
                        "id": 308,
                        "marking": "817_04",
                        "modelPart": 2,
                        "a": 77,
                        "b": 0,
                        "c": 57,
                        "d": 0,
                        "e": 20,
                        "f": 0,
                        "g": 0,
                        "comment": "Створка оконная 77мм"
                    },
                    {
                        "id": 290,
                        "marking": "150",
                        "modelPart": 4,
                        "a": 5,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель Н-образный"
                    },
                    {
                        "id": 298,
                        "marking": "337",
                        "modelPart": 3,
                        "a": 44,
                        "b": null,
                        "c": 24,
                        "d": 0,
                        "e": null,
                        "f": 0,
                        "g": 6,
                        "comment": "Импост оконный"
                    },
                    {
                        "id": 299,
                        "marking": "734",
                        "modelPart": 6,
                        "a": 32,
                        "b": null,
                        "c": 12,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Штульп оконный"
                    },
                    {
                        "id": 291,
                        "marking": "152",
                        "modelPart": 4,
                        "a": 20,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель Н-образный с армированием"
                    },
                    {
                        "id": 292,
                        "marking": "155",
                        "modelPart": 4,
                        "a": 149,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель угловой 90 градусов"
                    },
                    {
                        "id": 553,
                        "marking": "540+541",
                        "modelPart": 4,
                        "a": 182,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель с переменным углом"
                    },
                    {
                        "id": 296,
                        "marking": "144",
                        "modelPart": 4,
                        "a": 30,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 30 мм"
                    },
                    {
                        "id": 295,
                        "marking": "545",
                        "modelPart": 4,
                        "a": 45,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 45 мм"
                    },
                    {
                        "id": 294,
                        "marking": "546",
                        "modelPart": 4,
                        "a": 60,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 60 мм"
                    },
                    {
                        "id": 293,
                        "marking": "147",
                        "modelPart": 4,
                        "a": 120,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 120 мм"
                    },
                    {
                        "id": 754,
                        "marking": "Соединитель не определен",
                        "modelPart": 4,
                        "a": 50,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель не определен"
                    }
                ],
                "class": "Б",
                "brand": "KBE",
                "camernost": 3,
                "thickness": 58,
                "isDefault": false,
                "isEmpty": false,
                "isMoskitka": false,
                "isEnergy": null,
                "id": 16,
                "name": "KBE Эталон 58",
                "displayName": "КБЕ 58мм"
            },
            "profileSystemName": "KBE Эталон 58"
        },
        {
            "productGuid": "b5a9569a-1d69-4091-0ac9-b5044d5c059d",
            "frame": {
                "frameFillingGuid": "c94cdd80-2af6-3601-584c-8a2b026157c0",
                "shtulpOpenType": 0,
                "frameBeams": [
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 2605,
                            "y": 0
                        },
                        "end": {
                            "x": 1305,
                            "y": 0
                        },
                        "beamGuid": "851979b7-fa5f-076d-fd90-6a0a6ea64c21",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 2605,
                                "y": 0
                            },
                            {
                                "x": 1305,
                                "y": 0
                            },
                            {
                                "x": 1358,
                                "y": 53
                            },
                            {
                                "x": 2552,
                                "y": 53
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 2605,
                                "y": 0
                            },
                            {
                                "x": 1305,
                                "y": 0
                            },
                            {
                                "x": 1368,
                                "y": 63
                            },
                            {
                                "x": 2542,
                                "y": 63
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 1305,
                            "y": 0
                        },
                        "end": {
                            "x": 1305,
                            "y": 1400
                        },
                        "beamGuid": "89b195ce-7b5b-d534-7150-a7d1eaa30db9",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 1305,
                                "y": 0
                            },
                            {
                                "x": 1305,
                                "y": 1400
                            },
                            {
                                "x": 1358,
                                "y": 1347
                            },
                            {
                                "x": 1358,
                                "y": 53
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 1305,
                                "y": 0
                            },
                            {
                                "x": 1305,
                                "y": 1400
                            },
                            {
                                "x": 1368,
                                "y": 1337
                            },
                            {
                                "x": 1368,
                                "y": 63
                            }
                        ],
                        "_isConnectionPart": true,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 1305,
                            "y": 1400
                        },
                        "end": {
                            "x": 2605,
                            "y": 1400
                        },
                        "beamGuid": "67417188-7070-d984-af9a-c6e18811a049",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 1305,
                                "y": 1400
                            },
                            {
                                "x": 2605,
                                "y": 1400
                            },
                            {
                                "x": 2552,
                                "y": 1347
                            },
                            {
                                "x": 1358,
                                "y": 1347
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 1305,
                                "y": 1400
                            },
                            {
                                "x": 2605,
                                "y": 1400
                            },
                            {
                                "x": 2542,
                                "y": 1337
                            },
                            {
                                "x": 1368,
                                "y": 1337
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0"
                    },
                    {
                        "_width": 63,
                        "_id": null,
                        "start": {
                            "x": 2605,
                            "y": 1400
                        },
                        "end": {
                            "x": 2605,
                            "y": 0
                        },
                        "beamGuid": "47914264-4184-2321-adc1-4b68cf9653e8",
                        "modelPart": 1,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 2605,
                                "y": 1400
                            },
                            {
                                "x": 2605,
                                "y": 0
                            },
                            {
                                "x": 2552,
                                "y": 53
                            },
                            {
                                "x": 2552,
                                "y": 1347
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 2605,
                                "y": 1400
                            },
                            {
                                "x": 2605,
                                "y": 0
                            },
                            {
                                "x": 2542,
                                "y": 63
                            },
                            {
                                "x": 2542,
                                "y": 1337
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "807",
                        "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0"
                    }
                ],
                "impostBeams": [
                    {
                        "_width": 88,
                        "_id": null,
                        "start": {
                            "x": 1955,
                            "y": 0
                        },
                        "end": {
                            "x": 1955,
                            "y": 1400
                        },
                        "beamGuid": "4c6be6cf-cf46-1a4c-88ce-f243d926c0d7",
                        "modelPart": 3,
                        "userParameters": [],
                        "hardcodedUserParameters": [],
                        "_innerDrawingPoints": [
                            {
                                "x": 1989,
                                "y": 0
                            },
                            {
                                "x": 1921,
                                "y": 0
                            },
                            {
                                "x": 1921,
                                "y": 1400
                            },
                            {
                                "x": 1989,
                                "y": 1400
                            }
                        ],
                        "_outerDrawingPoints": [
                            {
                                "x": 1999,
                                "y": 0
                            },
                            {
                                "x": 1911,
                                "y": 0
                            },
                            {
                                "x": 1911,
                                "y": 1400
                            },
                            {
                                "x": 1999,
                                "y": 1400
                            }
                        ],
                        "_isConnectionPart": false,
                        "_hasInnerWidth": true,
                        "profileVendorCode": "337",
                        "shtulpType": 3,
                        "_dependencyIds": [
                            "67417188-7070-d984-af9a-c6e18811a049",
                            "851979b7-fa5f-076d-fd90-6a0a6ea64c21"
                        ],
                        "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0",
                        "_level": 0,
                        "_parentPolygon": {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1305,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1305,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1305,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1305,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                },
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 2605,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 2605,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 2605,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 2605,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ]
                        },
                        "_containerPolygon": {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 0,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1305,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1305,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1305,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1305,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                },
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 2605,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 0
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 0
                                            },
                                            "end": {
                                                "x": 1955,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 1955,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 2605,
                                                "y": 1400
                                            }
                                        },
                                        {
                                            "_width": 0,
                                            "_id": null,
                                            "start": {
                                                "x": 2605,
                                                "y": 1400
                                            },
                                            "end": {
                                                "x": 2605,
                                                "y": 0
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ]
                        }
                    }
                ],
                "innerFillings": [
                    {
                        "solid": {
                            "solidFillingGuid": "90a3b45f-4954-8a72-fa2f-e112f3a13411",
                            "userParameters": [
                                {
                                    "id": 1099,
                                    "value": "Нет",
                                    "title": "",
                                    "hex": ""
                                }
                            ],
                            "hardcodedUserParameters": [],
                            "_containerPolygon": {
                                "_id": null,
                                "_segments": [
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 1955,
                                            "y": 0
                                        },
                                        "end": {
                                            "x": 1305,
                                            "y": 0
                                        }
                                    },
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 1305,
                                            "y": 0
                                        },
                                        "end": {
                                            "x": 1305,
                                            "y": 1400
                                        }
                                    },
                                    {
                                        "_width": 53,
                                        "_id": null,
                                        "start": {
                                            "x": 1305,
                                            "y": 1400
                                        },
                                        "end": {
                                            "x": 1955,
                                            "y": 1400
                                        }
                                    },
                                    {
                                        "_width": 34,
                                        "_id": null,
                                        "start": {
                                            "x": 1955,
                                            "y": 1400
                                        },
                                        "end": {
                                            "x": 1955,
                                            "y": 0
                                        }
                                    }
                                ],
                                "childrenPolygons": []
                            },
                            "fillingPoint": {
                                "x": 1630,
                                "y": 700
                            },
                            "fillingVendorCode": "4-10-4-10-4",
                            "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0",
                            "filling": {
                                "isDefault": true,
                                "id": 27,
                                "name": "Стеклопакет 32 мм",
                                "marking": "4-10-4-10-4",
                                "thickness": 32,
                                "group": "Стеклопакеты",
                                "camernost": 2,
                                "isMultifunctional": false,
                                "isLowEmission": false
                            }
                        }
                    },
                    {
                        "leaf": {
                            "frameFillingGuid": "74346fd2-fe60-ce42-d9c5-39052e6b1700",
                            "shtulpOpenType": 0,
                            "frameBeams": [
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 2570,
                                        "y": 35
                                    },
                                    "end": {
                                        "x": 1971,
                                        "y": 35
                                    },
                                    "beamGuid": "7170ac2f-0cd0-573d-0346-2e2bc65bfd13",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 2570,
                                            "y": 35
                                        },
                                        {
                                            "x": 1971,
                                            "y": 35
                                        },
                                        {
                                            "x": 2038,
                                            "y": 102
                                        },
                                        {
                                            "x": 2503,
                                            "y": 102
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 2570,
                                            "y": 35
                                        },
                                        {
                                            "x": 1971,
                                            "y": 35
                                        },
                                        {
                                            "x": 2048,
                                            "y": 112
                                        },
                                        {
                                            "x": 2493,
                                            "y": 112
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "74346fd2-fe60-ce42-d9c5-39052e6b1700"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 1971,
                                        "y": 35
                                    },
                                    "end": {
                                        "x": 1971,
                                        "y": 1365
                                    },
                                    "beamGuid": "0acfe436-6792-75c1-d94c-9732dcbc8077",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 1971,
                                            "y": 35
                                        },
                                        {
                                            "x": 1971,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2038,
                                            "y": 1298
                                        },
                                        {
                                            "x": 2038,
                                            "y": 102
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 1971,
                                            "y": 35
                                        },
                                        {
                                            "x": 1971,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2048,
                                            "y": 1288
                                        },
                                        {
                                            "x": 2048,
                                            "y": 112
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "74346fd2-fe60-ce42-d9c5-39052e6b1700"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 1971,
                                        "y": 1365
                                    },
                                    "end": {
                                        "x": 2570,
                                        "y": 1365
                                    },
                                    "beamGuid": "11e876d3-9220-c9fd-b9a3-fb3704facd21",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 1971,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2570,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2503,
                                            "y": 1298
                                        },
                                        {
                                            "x": 2038,
                                            "y": 1298
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 1971,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2570,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2493,
                                            "y": 1288
                                        },
                                        {
                                            "x": 2048,
                                            "y": 1288
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "74346fd2-fe60-ce42-d9c5-39052e6b1700"
                                },
                                {
                                    "_width": 77,
                                    "_id": null,
                                    "start": {
                                        "x": 2570,
                                        "y": 1365
                                    },
                                    "end": {
                                        "x": 2570,
                                        "y": 35
                                    },
                                    "beamGuid": "d798aa6f-e697-f576-8ccc-3964d4c25d32",
                                    "modelPart": 2,
                                    "userParameters": [],
                                    "hardcodedUserParameters": [],
                                    "_innerDrawingPoints": [
                                        {
                                            "x": 2570,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2570,
                                            "y": 35
                                        },
                                        {
                                            "x": 2503,
                                            "y": 102
                                        },
                                        {
                                            "x": 2503,
                                            "y": 1298
                                        }
                                    ],
                                    "_outerDrawingPoints": [
                                        {
                                            "x": 2570,
                                            "y": 1365
                                        },
                                        {
                                            "x": 2570,
                                            "y": 35
                                        },
                                        {
                                            "x": 2493,
                                            "y": 112
                                        },
                                        {
                                            "x": 2493,
                                            "y": 1288
                                        }
                                    ],
                                    "_isConnectionPart": false,
                                    "_hasInnerWidth": true,
                                    "profileVendorCode": "817_04",
                                    "_parentNodeId": "74346fd2-fe60-ce42-d9c5-39052e6b1700"
                                }
                            ],
                            "impostBeams": [],
                            "innerFillings": [
                                {
                                    "solid": {
                                        "solidFillingGuid": "75069444-9623-ad93-19de-30d8bc379645",
                                        "userParameters": [
                                            {
                                                "id": 1099,
                                                "value": "Нет",
                                                "title": "",
                                                "hex": ""
                                            },
                                            {
                                                "id": 2698,
                                                "value": "Нет",
                                                "title": "Вклейка стеклопакета",
                                                "hex": ""
                                            }
                                        ],
                                        "hardcodedUserParameters": [],
                                        "_containerPolygon": {
                                            "_id": null,
                                            "_segments": [
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 2570,
                                                        "y": 35
                                                    },
                                                    "end": {
                                                        "x": 1971,
                                                        "y": 35
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 1971,
                                                        "y": 35
                                                    },
                                                    "end": {
                                                        "x": 1971,
                                                        "y": 1365
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 1971,
                                                        "y": 1365
                                                    },
                                                    "end": {
                                                        "x": 2570,
                                                        "y": 1365
                                                    }
                                                },
                                                {
                                                    "_width": 67,
                                                    "_id": null,
                                                    "start": {
                                                        "x": 2570,
                                                        "y": 1365
                                                    },
                                                    "end": {
                                                        "x": 2570,
                                                        "y": 35
                                                    }
                                                }
                                            ],
                                            "childrenPolygons": []
                                        },
                                        "fillingPoint": {
                                            "x": 2270.5,
                                            "y": 700
                                        },
                                        "fillingVendorCode": "4-10-4-10-4",
                                        "_parentNodeId": "74346fd2-fe60-ce42-d9c5-39052e6b1700",
                                        "filling": {
                                            "isDefault": true,
                                            "id": 27,
                                            "name": "Стеклопакет 32 мм",
                                            "marking": "4-10-4-10-4",
                                            "thickness": 32,
                                            "group": "Стеклопакеты",
                                            "camernost": 2,
                                            "isMultifunctional": false,
                                            "isLowEmission": false
                                        }
                                    }
                                }
                            ],
                            "moskitka": {
                                "set": false,
                                "color": "white",
                                "catproof": false
                            },
                            "handlePositionType": "center",
                            "handleColor": "Белый",
                            "userParameters": [
                                {
                                    "id": 413,
                                    "value": "Обычные",
                                    "title": "Петли оконные",
                                    "hex": ""
                                },
                                {
                                    "id": 416,
                                    "value": "Авто",
                                    "title": "Средний запор для Масо",
                                    "hex": ""
                                }
                            ],
                            "hardcodedUserParameters": [],
                            "_parentNodeId": "c94cdd80-2af6-3601-584c-8a2b026157c0",
                            "_containerPolygon": {
                                "_id": null,
                                "_segments": [
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 2570,
                                            "y": 35
                                        },
                                        "end": {
                                            "x": 1971,
                                            "y": 35
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 1971,
                                            "y": 35
                                        },
                                        "end": {
                                            "x": 1971,
                                            "y": 1365
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 1971,
                                            "y": 1365
                                        },
                                        "end": {
                                            "x": 2570,
                                            "y": 1365
                                        }
                                    },
                                    {
                                        "_width": 67,
                                        "_id": null,
                                        "start": {
                                            "x": 2570,
                                            "y": 1365
                                        },
                                        "end": {
                                            "x": 2570,
                                            "y": 35
                                        }
                                    }
                                ],
                                "childrenPolygons": []
                            },
                            "_fillingPolygons": [
                                {
                                    "_id": null,
                                    "_segments": [
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 2570,
                                                "y": 35
                                            },
                                            "end": {
                                                "x": 1971,
                                                "y": 35
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 1971,
                                                "y": 35
                                            },
                                            "end": {
                                                "x": 1971,
                                                "y": 1365
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 1971,
                                                "y": 1365
                                            },
                                            "end": {
                                                "x": 2570,
                                                "y": 1365
                                            }
                                        },
                                        {
                                            "_width": 67,
                                            "_id": null,
                                            "start": {
                                                "x": 2570,
                                                "y": 1365
                                            },
                                            "end": {
                                                "x": 2570,
                                                "y": 35
                                            }
                                        }
                                    ],
                                    "childrenPolygons": []
                                }
                            ],
                            "_isMoskitkaRoot": false,
                            "_isMoskitka": false,
                            "_isEmpty": false,
                            "_isEntranceDoor": false,
                            "_isAluminium": false,
                            "_isMetalDoor": false,
                            "fillingPoint": {
                                "x": 2270.5,
                                "y": 700
                            },
                            "openSide": 1,
                            "openType": 2,
                            "profile": {
                                "allowedThickness": [
                                    0,
                                    4,
                                    5,
                                    6,
                                    24,
                                    28,
                                    32
                                ],
                                "parts": [
                                    {
                                        "id": 287,
                                        "marking": "PR SP 751",
                                        "modelPart": 8,
                                        "a": 26,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Фальш-переплет на белой основе"
                                    },
                                    {
                                        "id": 310,
                                        "marking": "807",
                                        "modelPart": 1,
                                        "a": 63,
                                        "b": 0,
                                        "c": 43,
                                        "d": 0,
                                        "e": null,
                                        "f": 0,
                                        "g": 0,
                                        "comment": "Оконная рама"
                                    },
                                    {
                                        "id": 308,
                                        "marking": "817_04",
                                        "modelPart": 2,
                                        "a": 77,
                                        "b": 0,
                                        "c": 57,
                                        "d": 0,
                                        "e": 20,
                                        "f": 0,
                                        "g": 0,
                                        "comment": "Створка оконная 77мм"
                                    },
                                    {
                                        "id": 290,
                                        "marking": "150",
                                        "modelPart": 4,
                                        "a": 5,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель Н-образный"
                                    },
                                    {
                                        "id": 298,
                                        "marking": "337",
                                        "modelPart": 3,
                                        "a": 44,
                                        "b": null,
                                        "c": 24,
                                        "d": 0,
                                        "e": null,
                                        "f": 0,
                                        "g": 6,
                                        "comment": "Импост оконный"
                                    },
                                    {
                                        "id": 299,
                                        "marking": "734",
                                        "modelPart": 6,
                                        "a": 32,
                                        "b": null,
                                        "c": 12,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Штульп оконный"
                                    },
                                    {
                                        "id": 291,
                                        "marking": "152",
                                        "modelPart": 4,
                                        "a": 20,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель Н-образный с армированием"
                                    },
                                    {
                                        "id": 292,
                                        "marking": "155",
                                        "modelPart": 4,
                                        "a": 149,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель угловой 90 градусов"
                                    },
                                    {
                                        "id": 553,
                                        "marking": "540+541",
                                        "modelPart": 4,
                                        "a": 182,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель с переменным углом"
                                    },
                                    {
                                        "id": 296,
                                        "marking": "144",
                                        "modelPart": 4,
                                        "a": 30,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 30 мм"
                                    },
                                    {
                                        "id": 295,
                                        "marking": "545",
                                        "modelPart": 4,
                                        "a": 45,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 45 мм"
                                    },
                                    {
                                        "id": 294,
                                        "marking": "546",
                                        "modelPart": 4,
                                        "a": 60,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 60 мм"
                                    },
                                    {
                                        "id": 293,
                                        "marking": "147",
                                        "modelPart": 4,
                                        "a": 120,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Расширитель 120 мм"
                                    },
                                    {
                                        "id": 754,
                                        "marking": "Соединитель не определен",
                                        "modelPart": 4,
                                        "a": 50,
                                        "b": null,
                                        "c": null,
                                        "d": null,
                                        "e": null,
                                        "f": null,
                                        "g": null,
                                        "comment": "Соединитель не определен"
                                    }
                                ],
                                "class": "Б",
                                "brand": "KBE",
                                "camernost": 3,
                                "thickness": 58,
                                "isDefault": false,
                                "isEmpty": false,
                                "isMoskitka": false,
                                "isEnergy": null,
                                "id": 16,
                                "name": "KBE Эталон 58",
                                "displayName": "КБЕ 58мм"
                            },
                            "furniture": {
                                "id": 27,
                                "name": "Фурнитура Maco MM",
                                "displayName": "Maco MM",
                                "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                                "isDefault": false,
                                "isEmpty": false
                            },
                            "handleType": "МакБет"
                        }
                    }
                ],
                "moskitka": {
                    "set": false,
                    "color": "white",
                    "catproof": false
                },
                "handlePositionType": "center",
                "handleColor": "Белый",
                "userParameters": [],
                "hardcodedUserParameters": [],
                "_parentNodeId": null,
                "_containerPolygon": {
                    "_id": null,
                    "_segments": [
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 2605,
                                "y": 0
                            },
                            "end": {
                                "x": 1305,
                                "y": 0
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 1305,
                                "y": 0
                            },
                            "end": {
                                "x": 1305,
                                "y": 1400
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 1305,
                                "y": 1400
                            },
                            "end": {
                                "x": 2605,
                                "y": 1400
                            }
                        },
                        {
                            "_width": 53,
                            "_id": null,
                            "start": {
                                "x": 2605,
                                "y": 1400
                            },
                            "end": {
                                "x": 2605,
                                "y": 0
                            }
                        }
                    ],
                    "childrenPolygons": [
                        {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1955,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1305,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1305,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1955,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 34,
                                    "_id": null,
                                    "start": {
                                        "x": 1955,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 1955,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": []
                        },
                        {
                            "_id": null,
                            "_segments": [
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1955,
                                        "y": 0
                                    }
                                },
                                {
                                    "_width": 34,
                                    "_id": null,
                                    "start": {
                                        "x": 1955,
                                        "y": 0
                                    },
                                    "end": {
                                        "x": 1955,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 1955,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 1400
                                    }
                                },
                                {
                                    "_width": 53,
                                    "_id": null,
                                    "start": {
                                        "x": 2605,
                                        "y": 1400
                                    },
                                    "end": {
                                        "x": 2605,
                                        "y": 0
                                    }
                                }
                            ],
                            "childrenPolygons": []
                        }
                    ]
                },
                "_fillingPolygons": [
                    {
                        "_id": null,
                        "_segments": [
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1955,
                                    "y": 0
                                },
                                "end": {
                                    "x": 1305,
                                    "y": 0
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1305,
                                    "y": 0
                                },
                                "end": {
                                    "x": 1305,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1305,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 1955,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 34,
                                "_id": null,
                                "start": {
                                    "x": 1955,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 1955,
                                    "y": 0
                                }
                            }
                        ],
                        "childrenPolygons": []
                    },
                    {
                        "_id": null,
                        "_segments": [
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 2605,
                                    "y": 0
                                },
                                "end": {
                                    "x": 1955,
                                    "y": 0
                                }
                            },
                            {
                                "_width": 34,
                                "_id": null,
                                "start": {
                                    "x": 1955,
                                    "y": 0
                                },
                                "end": {
                                    "x": 1955,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 1955,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 2605,
                                    "y": 1400
                                }
                            },
                            {
                                "_width": 53,
                                "_id": null,
                                "start": {
                                    "x": 2605,
                                    "y": 1400
                                },
                                "end": {
                                    "x": 2605,
                                    "y": 0
                                }
                            }
                        ],
                        "childrenPolygons": []
                    }
                ],
                "_isMoskitkaRoot": false,
                "_isMoskitka": false,
                "_isEmpty": false,
                "_isEntranceDoor": false,
                "_isAluminium": false,
                "_isMetalDoor": false,
                "fillingPoint": {
                    "x": 1955,
                    "y": 700
                },
                "openSide": 4,
                "openType": 0,
                "profile": {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        28,
                        32
                    ],
                    "parts": [
                        {
                            "id": 287,
                            "marking": "PR SP 751",
                            "modelPart": 8,
                            "a": 26,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Фальш-переплет на белой основе"
                        },
                        {
                            "id": 310,
                            "marking": "807",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Оконная рама"
                        },
                        {
                            "id": 308,
                            "marking": "817_04",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная 77мм"
                        },
                        {
                            "id": 290,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 298,
                            "marking": "337",
                            "modelPart": 3,
                            "a": 44,
                            "b": null,
                            "c": 24,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 299,
                            "marking": "734",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 291,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 292,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 553,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 296,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 295,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 294,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 293,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 754,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "KBE",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 16,
                    "name": "KBE Эталон 58",
                    "displayName": "КБЕ 58мм"
                },
                "furniture": {
                    "id": 27,
                    "name": "Фурнитура Maco MM",
                    "displayName": "Maco MM",
                    "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                    "isDefault": false,
                    "isEmpty": false
                },
                "handleType": "МакБет"
            },
            "connectors": [],
            "heatTransferResistanceCoef": 0,
            "productOffset": 0,
            "userParameters": [
                {
                    "id": 822,
                    "value": "Авто",
                    "title": "Толщина армирования",
                    "hex": ""
                }
            ],
            "hardcodedUserParameters": [],
            "_refPoint": {
                "x": 1305,
                "y": 0
            },
            "_availableProfiles": [
                {
                    "allowedThickness": [
                        0,
                        1,
                        4,
                        5,
                        6,
                        24,
                        32,
                        36
                    ],
                    "parts": [
                        {
                            "id": 222,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 4,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 243,
                            "marking": "S358.19",
                            "modelPart": 6,
                            "a": 32,
                            "b": 0,
                            "c": 32,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 251,
                            "marking": "S358.02",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 253,
                            "marking": "S358.01",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": null,
                            "comment": "Рама оконная 63 мм"
                        },
                        {
                            "id": 257,
                            "marking": "S358.03",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": 0,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 224,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 26,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": 10,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 225,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": 58,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 239,
                            "marking": "#S-358.02",
                            "modelPart": 1,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама(створка) 77 мм"
                        },
                        {
                            "id": 220,
                            "marking": "#S-358.03",
                            "modelPart": 1,
                            "a": 82,
                            "b": null,
                            "c": 62,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама(импост) 82 мм"
                        },
                        {
                            "id": 223,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 95,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 935,
                            "marking": "#S358-01",
                            "modelPart": 3,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Импост из рамы для кабинок"
                        },
                        {
                            "id": 229,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 228,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": 45,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 227,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 60,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 226,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 120,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 250,
                            "marking": "S-358.26",
                            "modelPart": 2,
                            "a": 98,
                            "b": 0,
                            "c": 78,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": null,
                            "comment": "Створка дверная Z98"
                        },
                        {
                            "id": 756,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        },
                        {
                            "id": 930,
                            "marking": "S358.23",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": 58,
                            "d": null,
                            "e": 0,
                            "f": null,
                            "g": null,
                            "comment": "соединитель 358.23 (90 гр. Экспроф)"
                        }
                    ],
                    "class": "А",
                    "brand": "Exprof",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 14,
                    "name": "Exprof Practica 58mm",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        32,
                        40,
                        42,
                        44
                    ],
                    "parts": [
                        {
                            "id": 204,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 583,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 216,
                            "marking": "S571.11",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Коробка 63 мм"
                        },
                        {
                            "id": 219,
                            "marking": "S571.13",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        },
                        {
                            "id": 1078,
                            "marking": "S670.19",
                            "modelPart": 6,
                            "a": 20,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп для S670.19"
                        },
                        {
                            "id": 942,
                            "marking": "S571.22",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 205,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 582,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 203,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": 584,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 850,
                            "marking": "S571.23",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост усиленный"
                        },
                        {
                            "id": 472,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 100,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 473,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 202,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 589,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 643,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 590,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 760,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Exprof",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 13,
                    "name": "Exprof Profecta 70mm",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        28,
                        32
                    ],
                    "parts": [
                        {
                            "id": 287,
                            "marking": "PR SP 751",
                            "modelPart": 8,
                            "a": 26,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Фальш-переплет на белой основе"
                        },
                        {
                            "id": 310,
                            "marking": "807",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Оконная рама"
                        },
                        {
                            "id": 308,
                            "marking": "817_04",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная 77мм"
                        },
                        {
                            "id": 290,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 298,
                            "marking": "337",
                            "modelPart": 3,
                            "a": 44,
                            "b": null,
                            "c": 24,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 299,
                            "marking": "734",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 12,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 291,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 292,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 553,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 296,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 295,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 294,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 293,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 754,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "KBE",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 16,
                    "name": "KBE Эталон 58",
                    "displayName": "КБЕ 58мм"
                },
                {
                    "allowedThickness": [
                        0,
                        4,
                        5,
                        6,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 856,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с Н-образный"
                        },
                        {
                            "id": 350,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 351,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30"
                        },
                        {
                            "id": 352,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 354,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 417,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45 мм"
                        },
                        {
                            "id": 644,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 764,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        },
                        {
                            "id": 353,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с Н-образный с армированием"
                        },
                        {
                            "id": 349,
                            "marking": "PW 20104",
                            "modelPart": 4,
                            "a": 2,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 343,
                            "marking": "PW-0707 PW",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама 63 мм"
                        },
                        {
                            "id": 344,
                            "marking": "PW-0317 PW",
                            "modelPart": 2,
                            "a": 76,
                            "b": 0,
                            "c": 56,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка 77 мм"
                        },
                        {
                            "id": 345,
                            "marking": "PW-0132 PW",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        }
                    ],
                    "class": "",
                    "brand": "Plaswin",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": true,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 18,
                    "name": "Plaswin",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        40
                    ],
                    "parts": [
                        {
                            "id": 264,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 270,
                            "marking": "385.06",
                            "modelPart": 6,
                            "a": 32,
                            "b": 0,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 280,
                            "marking": "395",
                            "modelPart": 2,
                            "a": 77,
                            "b": 0,
                            "c": 57,
                            "d": 0,
                            "e": 20,
                            "f": 0,
                            "g": 0,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 282,
                            "marking": "390",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 286,
                            "marking": "392",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 281,
                            "marking": "70102",
                            "modelPart": 1,
                            "a": 69,
                            "b": 0,
                            "c": 49,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Рама дверная"
                        },
                        {
                            "id": 259,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 258,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель уговой 90 градусов"
                        },
                        {
                            "id": 2571,
                            "marking": "#392",
                            "modelPart": 1,
                            "a": 82,
                            "b": null,
                            "c": 62,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама из импоста"
                        },
                        {
                            "id": 265,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 262,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 261,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 260,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 759,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "А",
                    "brand": "KBE",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 15,
                    "name": "KBE Эксперт 70",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        1,
                        4,
                        5,
                        6,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 475,
                            "marking": "PR 1.063_T",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 476,
                            "marking": "PR 2.077_T",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 477,
                            "marking": "PR 3.082_T",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост оконный"
                        },
                        {
                            "id": 478,
                            "marking": "PR3.065T",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": 32,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп оконный"
                        },
                        {
                            "id": 482,
                            "marking": "150",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 483,
                            "marking": "152",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 485,
                            "marking": "155",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 487,
                            "marking": "540+541",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 490,
                            "marking": "144",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 492,
                            "marking": "545",
                            "modelPart": 4,
                            "a": 45,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 45"
                        },
                        {
                            "id": 494,
                            "marking": "147",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 493,
                            "marking": "546",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60"
                        },
                        {
                            "id": 933,
                            "marking": "S358.23",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "соединитель 358.23 (90 гр. Экспроф)"
                        },
                        {
                            "id": 768,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Proplex",
                    "camernost": 3,
                    "thickness": 58,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 25,
                    "name": "Proplex Litex",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32
                    ],
                    "parts": [
                        {
                            "id": 816,
                            "marking": "555 039",
                            "modelPart": 3,
                            "a": 38,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 0,
                            "comment": ""
                        },
                        {
                            "id": 817,
                            "marking": "#554 230",
                            "modelPart": 6,
                            "a": 31,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 819,
                            "marking": "639 571",
                            "modelPart": 4,
                            "a": 3,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель рамный 3/60"
                        },
                        {
                            "id": 814,
                            "marking": "555 009",
                            "modelPart": 1,
                            "a": 56,
                            "b": null,
                            "c": 38,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 815,
                            "marking": "555 029",
                            "modelPart": 2,
                            "a": 75,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 826,
                            "marking": "560 014 01",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный усиленный"
                        },
                        {
                            "id": 821,
                            "marking": "560 290 01",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90"
                        },
                        {
                            "id": 822,
                            "marking": "140 551 01 + 621 082 01",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Круглый соединитель с ответной частью"
                        },
                        {
                            "id": 824,
                            "marking": "561 004 01",
                            "modelPart": 4,
                            "a": 40,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 40 мм"
                        },
                        {
                            "id": 823,
                            "marking": "561 433 01",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 825,
                            "marking": "561 203 01",
                            "modelPart": 4,
                            "a": 100,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 100 мм"
                        },
                        {
                            "id": 820,
                            "marking": "732 460",
                            "modelPart": 4,
                            "a": 2,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 827,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "Б",
                    "brand": "Rehau",
                    "camernost": 3,
                    "thickness": 60,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 40,
                    "name": "Rehau Blitz New",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        36,
                        40
                    ],
                    "parts": [
                        {
                            "id": 833,
                            "marking": "PW70-3901",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 43,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 835,
                            "marking": "PW70-3951",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 837,
                            "marking": "PW70-3921",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 838,
                            "marking": "385",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 841,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 840,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 842,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 149,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 839,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 844,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 845,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 846,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 847,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "",
                    "brand": "Plaswin",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 41,
                    "name": "Plaswin 70",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        24,
                        32,
                        40
                    ],
                    "parts": [
                        {
                            "id": 877,
                            "marking": "546 301",
                            "modelPart": 4,
                            "a": 3,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель рамный 3/70"
                        },
                        {
                            "id": 878,
                            "marking": "560 024",
                            "modelPart": 4,
                            "a": 18,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Усилитель H-образный"
                        },
                        {
                            "id": 875,
                            "marking": "550 535",
                            "modelPart": 6,
                            "a": 32,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп 64мм"
                        },
                        {
                            "id": 873,
                            "marking": "505 600",
                            "modelPart": 3,
                            "a": 38,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 0,
                            "comment": "Импост 76мм"
                        },
                        {
                            "id": 870,
                            "marking": "505 510",
                            "modelPart": 1,
                            "a": 63,
                            "b": null,
                            "c": 45,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама 63мм"
                        },
                        {
                            "id": 871,
                            "marking": "505 700",
                            "modelPart": 2,
                            "a": 75,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка Z 55мм"
                        },
                        {
                            "id": 879,
                            "marking": "561 133",
                            "modelPart": 4,
                            "a": 190,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Угловой соединитель 90 град"
                        },
                        {
                            "id": 880,
                            "marking": "561 166 + 561 176",
                            "modelPart": 4,
                            "a": 186,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Круглый соединитель с ответной частью"
                        },
                        {
                            "id": 2577,
                            "marking": "560 303",
                            "modelPart": 4,
                            "a": 40,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 40мм"
                        },
                        {
                            "id": 882,
                            "marking": "550 130",
                            "modelPart": 4,
                            "a": 100,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 100мм"
                        },
                        {
                            "id": 883,
                            "marking": "561 116",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60мм"
                        }
                    ],
                    "class": "А",
                    "brand": "Rehau",
                    "camernost": 5,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 43,
                    "name": "Rehau Grazio",
                    "displayName": ""
                },
                {
                    "allowedThickness": [
                        0,
                        40,
                        42,
                        48
                    ],
                    "parts": [
                        {
                            "id": 1023,
                            "marking": "76105.07",
                            "modelPart": 1,
                            "a": 67,
                            "b": null,
                            "c": 46,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Рама оконная"
                        },
                        {
                            "id": 1026,
                            "marking": "76304.77",
                            "modelPart": 3,
                            "a": 42,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 1027,
                            "marking": "76402.06",
                            "modelPart": 6,
                            "a": 33,
                            "b": null,
                            "c": 11,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": ""
                        },
                        {
                            "id": 1029,
                            "marking": "76604",
                            "modelPart": 4,
                            "a": 6,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 1030,
                            "marking": "76605",
                            "modelPart": 4,
                            "a": 22,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 1031,
                            "marking": "8355+76821",
                            "modelPart": 4,
                            "a": 192,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 градусов"
                        },
                        {
                            "id": 1032,
                            "marking": "8340+8341+76821",
                            "modelPart": 4,
                            "a": 208,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 1033,
                            "marking": "76701",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 1034,
                            "marking": "76702",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 1035,
                            "marking": "76703",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 2153,
                            "marking": "76303",
                            "modelPart": -3,
                            "a": 55,
                            "b": null,
                            "c": 34,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": ""
                        },
                        {
                            "id": 1025,
                            "marking": "76209.67",
                            "modelPart": 2,
                            "a": 78,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        }
                    ],
                    "class": "Б",
                    "brand": "KÖMMERLING",
                    "camernost": 5,
                    "thickness": 76,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 46,
                    "name": "KBE 76",
                    "displayName": "Kömmerling 76"
                },
                {
                    "allowedThickness": [
                        0,
                        32,
                        40,
                        42,
                        44
                    ],
                    "parts": [
                        {
                            "id": 2599,
                            "marking": "S670.03",
                            "modelPart": 3,
                            "a": 41,
                            "b": null,
                            "c": 21,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": 6,
                            "comment": "Импост 82 мм"
                        },
                        {
                            "id": 2204,
                            "marking": "S571.23",
                            "modelPart": 3,
                            "a": 41,
                            "b": 0,
                            "c": 21,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 6,
                            "comment": "Импост усиленный"
                        },
                        {
                            "id": 2220,
                            "marking": "S670.19",
                            "modelPart": 6,
                            "a": 20,
                            "b": null,
                            "c": 20,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Штульп для S670.19"
                        },
                        {
                            "id": 2206,
                            "marking": "S670.01",
                            "modelPart": 1,
                            "a": 63,
                            "b": 0,
                            "c": 43,
                            "d": 0,
                            "e": null,
                            "f": 0,
                            "g": 0,
                            "comment": "Коробка 63 мм"
                        },
                        {
                            "id": 2209,
                            "marking": "350",
                            "modelPart": 4,
                            "a": 5,
                            "b": null,
                            "c": 583,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный"
                        },
                        {
                            "id": 2215,
                            "marking": "S670.02",
                            "modelPart": 2,
                            "a": 77,
                            "b": null,
                            "c": 57,
                            "d": null,
                            "e": 20,
                            "f": null,
                            "g": null,
                            "comment": "Створка оконная"
                        },
                        {
                            "id": 2210,
                            "marking": "352",
                            "modelPart": 4,
                            "a": 20,
                            "b": null,
                            "c": 582,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель Н-образный с армированием"
                        },
                        {
                            "id": 2208,
                            "marking": "355",
                            "modelPart": 4,
                            "a": 75,
                            "b": null,
                            "c": 584,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель угловой 90 гр."
                        },
                        {
                            "id": 2211,
                            "marking": "340+341",
                            "modelPart": 4,
                            "a": 182,
                            "b": null,
                            "c": 100,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель с переменным углом"
                        },
                        {
                            "id": 2212,
                            "marking": "360",
                            "modelPart": 4,
                            "a": 30,
                            "b": null,
                            "c": 30,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 30 мм"
                        },
                        {
                            "id": 2207,
                            "marking": "362",
                            "modelPart": 4,
                            "a": 60,
                            "b": null,
                            "c": 589,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 60 мм"
                        },
                        {
                            "id": 2213,
                            "marking": "363",
                            "modelPart": 4,
                            "a": 120,
                            "b": null,
                            "c": 590,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Расширитель 120 мм"
                        },
                        {
                            "id": 2214,
                            "marking": "Соединитель не определен",
                            "modelPart": 4,
                            "a": 50,
                            "b": null,
                            "c": null,
                            "d": null,
                            "e": null,
                            "f": null,
                            "g": null,
                            "comment": "Соединитель не определен"
                        }
                    ],
                    "class": "",
                    "brand": "Exprof",
                    "camernost": 6,
                    "thickness": 70,
                    "isDefault": false,
                    "isEmpty": false,
                    "isMoskitka": false,
                    "isEnergy": null,
                    "id": 98,
                    "name": "Exprof Experta 70mm",
                    "displayName": ""
                }
            ],
            "_availableFurniture": [
                {
                    "id": 1,
                    "name": "Фурнитура Roto NT",
                    "displayName": "Roto NT",
                    "vars": "140,280;190,481;283,601;433,801;533,1001;583,1201;1020,1801",
                    "isDefault": false,
                    "isEmpty": false
                },
                {
                    "id": 33,
                    "name": "Фурнитура Axor",
                    "displayName": "Axor",
                    "vars": "195,400;220,501;320,681;470,921;620,1161;1020,1881",
                    "isDefault": true,
                    "isEmpty": false
                },
                {
                    "id": 27,
                    "name": "Фурнитура Maco MM",
                    "displayName": "Maco MM",
                    "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                    "isDefault": false,
                    "isEmpty": false
                }
            ],
            "_availableFillings": [
                {
                    "isDefault": false,
                    "id": 89,
                    "name": "Стекло 4 мм Матовое",
                    "marking": "4 W",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 303,
                    "name": "4мм Stemalit RAL8017",
                    "marking": "4мм Stemalit RAL8017",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 63,
                    "name": "Зеркало 4 мм",
                    "marking": "4мм Зеркало",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 73,
                    "name": "Стекло 4 мм М1",
                    "marking": "4мм Стекло",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 76,
                    "name": "стекло 4 мм М1 Solar",
                    "marking": "ClimaGuard Solar 4.00 мм",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 121,
                    "name": "Стекло Комфорт серебристо-серый 40, 4мм",
                    "marking": "Comfort Silver Gr 40, 4мм",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 75,
                    "name": "стекло 4 мм М1 Bronze",
                    "marking": "SunGuard HP Light Bronze",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 19,
                    "name": "SunGuard HP Royal Blue 38",
                    "marking": "SunGuard HP Royal Blue 38",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 74,
                    "name": "стекло 4 мм M1 Silver",
                    "marking": "SunGuard HP Silver 35/26",
                    "thickness": 4,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 5,
                    "name": "Без заполнения 4 мм",
                    "marking": "Без заполнения 4 мм",
                    "thickness": 4,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 262,
                    "name": "Вентрешетка 4мм Тендер (ФС)",
                    "marking": "Вентрешетка 4мм Тендер (ФС)",
                    "thickness": 4,
                    "group": "Непрозрачное заполнение",
                    "camernost": 0,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 20,
                    "name": "Стекло 5 мм",
                    "marking": "5мм Стекло",
                    "thickness": 5,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 7,
                    "name": "Без заполнения 5 мм",
                    "marking": "Без заполнения 5 мм",
                    "thickness": 5,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 116,
                    "name": "Стекло 6 мм Тонированное в массе",
                    "marking": "6 Т",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 161,
                    "name": "Стекло 6 мм RAL 9003",
                    "marking": "6 мм RAL 9003",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 92,
                    "name": "стекло 6 мм закаленное PlanibelGrey",
                    "marking": "6зак PlanibelGrey",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 194,
                    "name": "стекло 6 мм ST Ph Bronze зак. шлиф.",
                    "marking": "6зак ST Ph Bronze",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 159,
                    "name": "стекло 6 мм СМ3 Голубое 20 Зак шлиф (штамп)",
                    "marking": "6зак ST blue Vision 50T",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 105,
                    "name": "6зак SunGuard HD Black",
                    "marking": "6зак SunGuard HD Black",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 104,
                    "name": "6зак SunGuard HD Grey",
                    "marking": "6зак SunGuard HD Grey",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 141,
                    "name": "стекло 6 мм СМ3 Голубое 20 Зак шлиф (штамп)",
                    "marking": "6зак Голубое СМ3",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 44,
                    "name": "Стекло 6 мм Triplex",
                    "marking": "6мм Triplex",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 143,
                    "name": "стекло 6 мм СМ3 Голубое 20",
                    "marking": "6мм Голубое СМ3",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 21,
                    "name": "Стекло 6 мм",
                    "marking": "6мм Стекло",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 168,
                    "name": "Cтекло 6 мм Matelux clear зак.",
                    "marking": "6мм зак Matelux clear",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 78,
                    "name": "стекло 6 мм М1 Solar",
                    "marking": "ClimaGuard Solar 6.00 мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 91,
                    "name": "SunGuard HD blue 6 мм",
                    "marking": "SunGuard HD blue 6 мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 77,
                    "name": "стекло 6 мм М1 Bronze",
                    "marking": "SunGuard HP Light Bronze6",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 8,
                    "name": "Без заполнения 6 мм",
                    "marking": "Без заполнения 6 мм",
                    "thickness": 6,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 122,
                    "name": "стекло Комфорт голубой 40, 6мм",
                    "marking": "Комфорт голубой 40, 6мм",
                    "thickness": 6,
                    "group": "Стекла",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 57,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4 Ц-16-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 252,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4 Ц-16-4 i",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 119,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 54,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6 Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 280,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-14-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 6,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 55,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4 i",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 56,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16-4 Т",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 305,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-16Ar-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 106,
                    "name": "Стеклопакет 24 мм",
                    "marking": "4-6-4-6-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 132,
                    "name": "Стеклопакет 24 мм",
                    "marking": "5-14-5",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 99,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6 Т[A1]-12Ar-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 250,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-10-8мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 66,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-12-6",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 278,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-12-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 128,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6-14-4",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 214,
                    "name": "Стеклопакет 24 мм",
                    "marking": "6мм Triplex-12-6мм Triplex",
                    "thickness": 24,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 11,
                    "name": "Без заполнения 24 мм",
                    "marking": "Без заполнения 24 мм",
                    "thickness": 24,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 85,
                    "name": "Пенополистирол 24 мм с оцинковкой",
                    "marking": "Пенополистирол 24 мм с оцинковкой",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 1,
                    "name": "Сэндвич 24 мм",
                    "marking": "Сэндвич 24 мм",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 94,
                    "name": "Сэндвич 24 мм",
                    "marking": "Сэндвич 24 мм с оцинковкой с внутр стороны",
                    "thickness": 24,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 231,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4 Ц-8-4-8-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 109,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-20-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 283,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-6-4-10-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 139,
                    "name": "Стеклопакет 28 мм",
                    "marking": "4-8-4-8-4",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 215,
                    "name": "Стеклопакет 28 мм",
                    "marking": "6-16-6",
                    "thickness": 28,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 47,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4 Ц-10-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": true,
                    "id": 27,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 43,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 46,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-10-4 Т",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 107,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10-4-8-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 48,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-10Ar-4-10Ar-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 123,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-12-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 38,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-24-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 59,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-24-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 289,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-6-4-14-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 142,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-8-4-10-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 40,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-8-4-12-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 145,
                    "name": "Стеклопакет 32 мм",
                    "marking": "4-9-4-9-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 241,
                    "name": "Стеклопакет 32 мм",
                    "marking": "5-8-4-10-5",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 229,
                    "name": "Стеклопакет 32 мм",
                    "marking": "5-9-4-9-5",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 69,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6 Ц-8-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": true,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 144,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-10-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 180,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-10-4-8-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 130,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-20-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 222,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-20-6 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 120,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-4-10-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 134,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-4-8-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 275,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-6-6-6",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 163,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-8-6-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 164,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6-9-4-9-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 71,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6hard-20-6 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 238,
                    "name": "Стеклопакет 32 мм",
                    "marking": "6мм Triplex-20-6мм Triplex",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 1,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 258,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8зак-8-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 223,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8мм Triplex-8-4-8-4",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 224,
                    "name": "Стеклопакет 32 мм",
                    "marking": "8мм Triplex-8-4-8-4 i",
                    "thickness": 32,
                    "group": "Стеклопакеты",
                    "camernost": 2,
                    "isMultifunctional": false,
                    "isLowEmission": true
                },
                {
                    "isDefault": false,
                    "id": 12,
                    "name": "Без заполнения 32 мм",
                    "marking": "Без заполнения 32 мм",
                    "thickness": 32,
                    "group": "Без заполнения",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 4,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 95,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм с оцинковкой с внеш стороны",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                },
                {
                    "isDefault": false,
                    "id": 96,
                    "name": "Сэндвич 32 мм",
                    "marking": "Сэндвич 32 мм с оцинковкой с внутр стороны",
                    "thickness": 32,
                    "group": "Сэндвичи",
                    "camernost": 0,
                    "isMultifunctional": false,
                    "isLowEmission": false
                }
            ],
            "constructionTypeId": 2,
            "hardwareSystemName": "Фурнитура Maco MM",
            "insideColorId": 2,
            "outsideColorId": 2,
            "productionTypeId": 898,
            "profileSystemName": "KBE Эталон 58",
            "profile": {
                "allowedThickness": [
                    0,
                    4,
                    5,
                    6,
                    24,
                    28,
                    32
                ],
                "parts": [
                    {
                        "id": 287,
                        "marking": "PR SP 751",
                        "modelPart": 8,
                        "a": 26,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Фальш-переплет на белой основе"
                    },
                    {
                        "id": 310,
                        "marking": "807",
                        "modelPart": 1,
                        "a": 63,
                        "b": 0,
                        "c": 43,
                        "d": 0,
                        "e": null,
                        "f": 0,
                        "g": 0,
                        "comment": "Оконная рама"
                    },
                    {
                        "id": 308,
                        "marking": "817_04",
                        "modelPart": 2,
                        "a": 77,
                        "b": 0,
                        "c": 57,
                        "d": 0,
                        "e": 20,
                        "f": 0,
                        "g": 0,
                        "comment": "Створка оконная 77мм"
                    },
                    {
                        "id": 290,
                        "marking": "150",
                        "modelPart": 4,
                        "a": 5,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель Н-образный"
                    },
                    {
                        "id": 298,
                        "marking": "337",
                        "modelPart": 3,
                        "a": 44,
                        "b": null,
                        "c": 24,
                        "d": 0,
                        "e": null,
                        "f": 0,
                        "g": 6,
                        "comment": "Импост оконный"
                    },
                    {
                        "id": 299,
                        "marking": "734",
                        "modelPart": 6,
                        "a": 32,
                        "b": null,
                        "c": 12,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Штульп оконный"
                    },
                    {
                        "id": 291,
                        "marking": "152",
                        "modelPart": 4,
                        "a": 20,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель Н-образный с армированием"
                    },
                    {
                        "id": 292,
                        "marking": "155",
                        "modelPart": 4,
                        "a": 149,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель угловой 90 градусов"
                    },
                    {
                        "id": 553,
                        "marking": "540+541",
                        "modelPart": 4,
                        "a": 182,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель с переменным углом"
                    },
                    {
                        "id": 296,
                        "marking": "144",
                        "modelPart": 4,
                        "a": 30,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 30 мм"
                    },
                    {
                        "id": 295,
                        "marking": "545",
                        "modelPart": 4,
                        "a": 45,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 45 мм"
                    },
                    {
                        "id": 294,
                        "marking": "546",
                        "modelPart": 4,
                        "a": 60,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 60 мм"
                    },
                    {
                        "id": 293,
                        "marking": "147",
                        "modelPart": 4,
                        "a": 120,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Расширитель 120 мм"
                    },
                    {
                        "id": 754,
                        "marking": "Соединитель не определен",
                        "modelPart": 4,
                        "a": 50,
                        "b": null,
                        "c": null,
                        "d": null,
                        "e": null,
                        "f": null,
                        "g": null,
                        "comment": "Соединитель не определен"
                    }
                ],
                "class": "Б",
                "brand": "KBE",
                "camernost": 3,
                "thickness": 58,
                "isDefault": false,
                "isEmpty": false,
                "isMoskitka": false,
                "isEnergy": null,
                "id": 16,
                "name": "KBE Эталон 58",
                "displayName": "КБЕ 58мм"
            },
            "furniture": {
                "id": 27,
                "name": "Фурнитура Maco MM",
                "displayName": "Maco MM",
                "vars": "145,270;210,431;320,661;420,841;520,1091;620,1341;720,1591;1070,1701",
                "isDefault": false,
                "isEmpty": false
            },
            "outsideLamination": {
                "id": 2,
                "name": "Белый",
                "hex": "#fff"
            },
            "insideLamination": {
                "id": 2,
                "name": "Белый",
                "hex": "#fff"
            }
        }
    ],
    "estShippingDate": null,
    "errors": [],
    "connectorsParameters": [
        {
            "id": 296,
            "marking": "144",
            "modelPart": 4,
            "a": 30,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Расширитель 30 мм",
            "isDefault": false,
            "connectorType": "extender"
        },
        {
            "id": 293,
            "marking": "147",
            "modelPart": 4,
            "a": 120,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Расширитель 120 мм",
            "isDefault": false,
            "connectorType": "extender"
        },
        {
            "id": 290,
            "marking": "150",
            "modelPart": 4,
            "a": 5,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Соединитель Н-образный",
            "isDefault": false,
            "connectorType": "connector"
        },
        {
            "id": 291,
            "marking": "152",
            "modelPart": 4,
            "a": 20,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Соединитель Н-образный с армированием",
            "isDefault": false,
            "connectorType": "connector"
        },
        {
            "id": 292,
            "marking": "155",
            "modelPart": 4,
            "a": 149,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Соединитель угловой 90 градусов",
            "isDefault": false,
            "connectorType": "connector"
        },
        {
            "id": 553,
            "marking": "540+541",
            "modelPart": 4,
            "a": 182,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Соединитель с переменным углом",
            "isDefault": false,
            "connectorType": "connector"
        },
        {
            "id": 295,
            "marking": "545",
            "modelPart": 4,
            "a": 45,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Расширитель 45 мм",
            "isDefault": false,
            "connectorType": "extender"
        },
        {
            "id": 294,
            "marking": "546",
            "modelPart": 4,
            "a": 60,
            "b": null,
            "c": null,
            "d": null,
            "e": null,
            "f": null,
            "g": null,
            "comment": "Расширитель 60 мм",
            "isDefault": false,
            "connectorType": "extender"
        }
    ],
    "coordinateSystem": "absolute",
    "defaultPointOfView": "inside",
    "haveEnergyProducts": false,
    "energyProductType": "",
    "calculatedWithFramerPoints": false,
    "hasErrors": false
};